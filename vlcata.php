<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Vlčata</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/vlcata.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Vlčata</b><br><br>
                        <p class="pageData">
                            Do družiny chodí kluci navštěvující 2. až 5. třídu základní školy. <br><br> Program družiny 
                            je přizpůsoben potřebám a věku chlapců. Od září do června se scházíme každý týden na družinových schůzkách, 
                            podnikáme také různé výpravy. V létě vyrážíme společně s ostatními družinami oddílu na 14-ti denní letní tábor. <br><br> Pro případné přihlášení 
                            dětí kontaktujte jednoho z rádců nebo přijďte na naši schůzku. <br><br> 
                            <b class="schuzkyDruziny">Schůzky družiny</b><br>Pondělí 16:00 - 17:30 <br>
                            Schůzky začínají před <a class="lokalitaSchuzek" href="naseKlubovny">naší klubovnou</a>, nacházející se v 1. patře hasičky <br><br>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center"><b class="nazevDruziny">Vedoucí družiny</b></p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/lukas.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Lukáš Prokeš</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 730 695 355 <br>
                                    e-mail: lukas.prokes03@gmail.com
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/pavel.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Pavel Staněk</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 723 010 760 <br>
                                    e-mail: pavels@skaut.cz
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php")?>
    </div>
  </body>
</html>