<?php 

require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

function sendEmail($address,$subject,$EmailBody,$EmailFooter=""){
    $mail = new PHPMailer; 

    $from = ""; //Zde vložte školní e-mailovou adresu
    $password = ""; //Heslo pro přihlášení
        
    $mail->isSMTP();                      // Set mailer to use SMTP 
    $mail->Host = 'smtp.office365.com';       // Specify main and backup SMTP servers 
    $mail->SMTPAuth = true;               // Enable SMTP authentication 
    $mail->Username = $from;   // SMTP username 
    $mail->Password = $password;   // SMTP password 
    $mail->SMTPSecure = 'starttls';            // Enable TLS encryption, `ssl` also accepted 
    $mail->Port = 587;                    // TCP port to connect to 
    $mail->CharSet = 'UTF-8';

    // Sender info 
    $mail->setFrom($from, 'Středisko Devíti křížů Domašov'); 

    // Add a recipient 
    $mail->addAddress($address);

    // Set email format to HTML 
    $mail->isHTML(true); 

    // Mail subject 
    $mail->Subject = $subject;

    $bodyContent = '<!DOCTYPE html>
    <html lang="cs">
        <head>
        <style>
        @font-face {
            font-family: "skautbold";
            src: url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.eot");
            src: url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.eot? #iefix") format("embedded-opentype");
            src: url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.woff") format("woff"),
                    url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.woff2") format("woff2"),
                 url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.otf") format("opentype"),
                 url("https://cdn.skauting.cz/fonts/skaut-bold-webfont.svg#skautbold") format("svg");
            font-weight: normal;
            font-style: normal;
        }
        .logoText {
            display: inline-block;
            vertical-align: middle;
            font-family: skautbold;
            margin-left: 1.5rem;
            margin-bottom:0;
            
        }
        @media only screen and (max-width:1300px) {
            .logoText{
                font-size:2vw;
            }
            .logo_header{
                width:10%;
            }
        }
        @media only screen and (min-width: 1300px) {
            .logoText{
                font-size:1.1vw;
            }
            .logo_header{
                width:7%;
            }
        }
        </style>
        </head>
        <body>
            <div style="width:75%;margin:auto;overflow: hidden;">
                <div>
                    <img src="https://kokes.4fan.cz/public/img/SKAUT_logo.png" class="logo_header" style="vertical-align: middle;" alt="logo">

                    <div class="logoText">
                        <h1 style="margin-bottom: 0 !important;">Středisko Devíti křížů</h1>
                        <h2 style="margin-top: 0.8rem;">Domašov</h2>
                    </div>
                </div>
                <hr style="margin:0.3rem 0 1.2rem 0;">
                <div style="display: inline-block;width:100%;">
                    '.$EmailBody.'
                </div>
                <hr style="margin: 2rem 0 1rem 0;">
                <p style="text-align: center !important;">Copyright © 2021-'.date("Y").', Středisko Devíti křížů Domašov '.$EmailFooter.'</p>
            </div>
        </body>
    </html>';     

    $mail->Body = $bodyContent; 
    // Send email 
    if(!$mail->send()) { 
        return false;
    } else { 
        return true; 
    }
}
?>
