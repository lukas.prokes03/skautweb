<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<!-- MDB icon -->
<link rel="icon" href="/public/img/SKAUT_logo.png"  />
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
<!-- Google Fonts Roboto -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"/>
<!-- MDB -->
<link rel="stylesheet" href="/public/css/mdb.min.css" /> <!-- MD Bootstrap CSS -->
<link rel="stylesheet" href="/public/css/main.css" /> <!-- Custom CSS -->
<link rel="stylesheet" href="/public/css/fonts.css" /> <!-- Custom fonts -->