<?php
session_start();

$webDomainName = "http://localhost";

//Kontrola zda je uživatel přihlášen
function isLogged(){
    if( isset( $_SESSION['user_id'] )){
        return true;
    }else{
        return false;
    }
}
//Když je uživatel přihlášen vrací jeho id
function getUserId(){
    if( isset( $_SESSION['user_id'] )){
        return $_SESSION['user_id'];
    }else{
        return null;
    }
}
//Když je uživatel přihlášen vrací jeho email
function getUserEmail(){
    if( isset( $_SESSION['user_id'] )){
        return $_SESSION['email'];
    }else{
        return null;
    }
}
//Kontrola jestli je uživatel admin
function isAdmin(){
    if(isLogged()){
        if($_SESSION['role'] == 1){
            return true;
        }elseif($_SESSION['role'] == 2){
            return true;
        }else{
            return false;
        }
    }
}
//Kontrola jestli je uživatel root
function isRoot(){
    if(isLogged()){
        if($_SESSION['role'] == 2){
            return true;
        }else{
            return false;
        }
    }
}
//Když není uživatel admin dostane 403 na stránkach pro adminy
function adminKick(){
    if(!isAdmin()){
        Header("Location : ../client/adminLogin");
        exit;
    }
}
//Když není uživatel root dostane 403 na stránkach pro adminy
function rootKick(){
    if(!isRoot()){
        Header("Location : ../client/adminLogin");
        //http_response_code(403);
        exit;
    }
}
function loginKick(){
    if(!isLogged()){
        Header("Location : ../");
    }
}
//Vrací celé jméno uživatele
function getFullName(){
    if(isLogged()){
        return $_SESSION["firstname"] . " " . $_SESSION["lastname"];
    }
}
?>