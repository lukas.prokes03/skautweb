<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <link rel="stylesheet" href="public/css/carousel.css" />
    <title>Skaut Domašov | Kontakt</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <p class="nazev_klubovny">Louka pod Domašovem</p>
                <div class="row">
                    <div class="col-md-6" >
                        <p>Pod Domašovem, v lesích v údolí potůčku Bobravy máme svoji louku. Využíváme ji hlavně na větší hry či jako útočiště při různých 
                            akcích a schůzkách.<br><br>Lze ji po domluvě poskytnout i jako tábořiště, nicméně je potřeba vzít v potaz, že nejbližší zdroj pitné 
                            vody je asi 400m a v potůčku se dá koupat a mýt jen s obtížemi (nejbližší koupání je rybník Bahňák vzdálený přibližně 3km). Na 
                            louce je možné tábořit jen s vlastním vybavením.<br><br>
                            Kontakt: Mirek Staněk, skaut.domasov@seznam.cz
                        </p>
                    </div>
                    <div class="col-md-6">
                        <iframe style="border:none" src="https://en.frame.mapy.cz/s/pamadavoko" width="100%" height="450px" frameborder="0"></iframe>               
                    </div>
                </div>
                <div style="width:80%;margin:auto;padding-bottom: 4rem;">
                    <div class="slideshow-container fade">
                        <div class="Containers">
                            <img src="public/img/louka_domasov/loukaPodDomasovem1.jpg" style="width:100%">
                        </div>
                        <a class="Back" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="forward" onclick="plusSlides(1)">&#10095;</a>
                    </div><br>
                    <div style="text-align:center">
                        <span class="dots" onclick="currentSlide(1)"></span>
                    </div> 
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
</body>
<script>
    var slidePosition = 1;
    SlideShow(slidePosition);
    function plusSlides(n) {
        SlideShow(slidePosition += n);
    }
    function currentSlide(n) {
        SlideShow(slidePosition = n);
    }
    function SlideShow(n) {
        var i;
        var slides = document.getElementsByClassName("Containers");
        var circles = document.getElementsByClassName("dots");
        if (n > slides.length) {slidePosition = 1}
        if (n < 1) {slidePosition = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < circles.length; i++) {
            circles[i].className = circles[i].className.replace(" enable", "");
        }
        slides[slidePosition-1].style.display = "block";
        circles[slidePosition-1].className += " enable";
    } 
</script>
</html>