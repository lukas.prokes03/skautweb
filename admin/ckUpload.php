<?php 
include("config.php");
include("../imports/client.php");
adminKick();

error_reporting(E_ERROR | E_PARSE);

function uploadFile(){
    $target_dir = "/public/img/article";
    $target_file = $target_dir . basename($_FILES["upload"]["name"]);

    $response = new stdClass();
    $response->uploaded = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $random_name = basename($_FILES["upload"]["name"]);

    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["upload"]["tmp_name"]);
    if($check !== false) {
        $response->uploaded = 1;
    } else {
        $response->error->message = "Nahrávání se nezdařilo, zkuste to prosím znovu.";
        $response->uploaded = 0;
        }
    }

    // Check file size
    if ($_FILES["upload"]["size"] > 20000000) {
        $response->error->message = "Byl nahrán moc veliký obrázek.";
        $response->uploaded = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
    $response->error->message = "Byl nahrán nepodporovaný typ obrázku.";
    $response->uploaded = 0;
    }

    $target_file = realpath(dirname("../../")) . "/" . $target_dir . "/" . $random_name;
    $response->fileName = $random_name;

    // Check if $uploadOk is set to 0 by an error
    if ($response->uploaded == 0) {
    } else {
        if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
            $newFilePath = ".." . $target_dir . "/"  . $random_name;
            $response->url = $newFilePath;
        } else {
            $response->error->message = "Nahrávání se nezdařilo, zkuste to prosím znovu.";
        }
    }

    $responseJSON = json_encode($response);
    return $responseJSON;
}
echo (uploadImage());

function uploadImage(){
    $target_dir = "../public/img/article/";

    $response = new stdClass();
    $response->uploaded = 1;

    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($_FILES["upload"]["name"],PATHINFO_EXTENSION));
    $imageName = generateRandomName();
    $target_file = $target_dir . $imageName .".". $imageFileType;
    
    //Kontrola existence $target_dir
    if (!file_exists($target_dir)) {
        $uploadOk = 0;
        $response->error->message = "Složka do které se snažíte uložit obrázek neexistuje";
        $response->uploaded = 0;
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = 0;
        $response->error->message = "Soubor již existuje";
        $response->uploaded = 0;
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $uploadOk = 0;
        $response->error->message = "Byl nahrán nepodporovaný typ obrázku";
        $response->uploaded = 0;
    }

    $response->url = $target_file;
    $response->fileName = $imageName.'.'.$imageFileType;
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $response->uploaded = 0;
    } else {
        if($imageFileType == "jpg" || $imageFileType == "jpeg" ) {
            $orientation = exif_read_data($_FILES["upload"]["tmp_name"])["Orientation"];
        }else{
            $orientation = 1;
        }

        list($width, $height, $type) = getimagesize($_FILES["upload"]["tmp_name"]);
        $old_image = load_image($_FILES["upload"]["tmp_name"], $type);
        
        if ($width>$height){
            $image_fixed = resize_image_to_width(700, $old_image, $width, $height);
        }else{
            $image_fixed = resize_image_to_height(700, $old_image, $width, $height);
        }
        imagejpeg($image_fixed, $target_file, 100);
        correctImageOrientation($target_file,$orientation);
        imagedestroy($image_fixed);
    }
    $responseJSON = json_encode($response);
    return $responseJSON;
}

function correctImageOrientation($filename,$orientation) {
    if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
        case 3:
            $deg = 180;
            break;
        case 6:
            $deg = 270;
            break;
        case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
        $img = imagerotate($img, $deg, 0);        
        }
        imagejpeg($img, $filename, 95);
    }  
}

function load_image($filename, $type) {
    if( $type == IMAGETYPE_JPEG ) {
        $image = imagecreatefromjpeg($filename);
    }
    elseif( $type == IMAGETYPE_PNG ) {
        $image = imagecreatefrompng($filename);
    }
    elseif( $type == IMAGETYPE_GIF ) {
        $image = imagecreatefromgif($filename);
    }
    return $image;
}

function resize_image($new_width, $new_height, $image, $width, $height) {
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    return $new_image;
}

function resize_image_to_width($new_width, $image, $width, $height) {
    $resize_ratio = $new_width / $width;
    $new_height = $height * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function resize_image_to_height($new_height, $image, $width, $height) {
    $resize_ratio = $new_height / $height;
    $new_width = $width * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function generateRandomName(){
    $length = 8;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $real_string_length = strlen($characters) ;     
    $string="";

    for ($p = 0; $p < $length; $p++){
        $string .= $characters[mt_rand(0, $real_string_length-1)];
    }
    return $string;
}

?>