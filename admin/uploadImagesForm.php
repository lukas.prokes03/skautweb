<?php 
include ("../imports/client.php");
adminKick();
?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("../imports/head.php") ?>
    <title>Skaut Domašov | Kontakt</title>
    <style>
        .uploadImageFormTextInput{
            background-color: #ffffff;
            border: 2px solid #ccc;
            margin-top:2rem;
            margin-bottom:1rem;
            width:250px;
            height:35px;
            font-size:1.1rem;
            color:#383838;
            border-radius: 2px;

            display: block;
            width: 100%;
        }
        .uploadImageFormText{
            margin-bottom:0.2rem;
            font-size:1.1rem;

            display: inline-block;
        }
        .file {
        opacity: 0;
        width: 0.1px;
        height: 0.1px;
        position: absolute;
        }
        .file-input{
            float: right;
        }
        .file-input label {
        display: block;
        position: relative;
        width: 130px;
        border-radius: 5px;
        background: #F6EBD8;
        box-shadow: 0 3px 5px rgba(0, 0, 0, 0.4);
        display: flex;
        align-items: center;
        justify-content: center;
        color: #707070;
        font-weight: bold;
        cursor: pointer;
        transition: transform .2s ease-out;
        }
        input:hover + label,
        input:focus + label {
        transform: scale(1.02);
        }
        .file-name {
        position: absolute;
        color: #707070;
        max-width: 130px;
        max-height: 27px;
        overflow: hidden;
        }
        .uploadFormSubmitButton{
            background-color: #74cd6c;
            color: white;
            display: flex;
        justify-content: center;
        align-items: center;
        }
        .form-select{
            border: 2px solid #ccc; 
            margin-top:2rem;
        }
        @media only screen and  (max-width:767px) {
            #typeOfUploadButtons{
                width:95%;
            }
            #uploadImagesForm{
                width:95%;
            }
        }
        @media only screen and (min-width:768px) and  (max-width:1195px) {
            #typeOfUploadButtons{
                width:75%;
            }
            #uploadImagesForm{
                width:75%;
            }
        }
        
        @media only screen and  (min-width:1196px) {
            #typeOfUploadButtons{
                width:60%;
            }
            #uploadImagesForm{
                width:60%;
            }
        }
    </style>
  </head>
  <body>
    <?php include("../imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("../view/indexHeader.php");?>
            <div class="page_container">
                <div class="row mb-3" id="typeOfUploadButtons" style="margin:auto">
                    <div class="col-4">
                        <button class="btn btn-submit uploadFormButton" id="newImageFormButton" onclick="getNewImageForm()">Nahrát obrázky</button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-submit uploadFormButton" id="newAlbumFormButton" onclick="getNewAlbumForm()">Vytvořit album</button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-submit uploadFormButton" onclick="getNewRokAlbumForm()">Vytvořit rok</button>
                    </div>
                </div>

                <div class="row">
                    <div id="uploadImagesForm" style="margin:auto">
                        <form > 
                            <div id="newImageForm"></div>

                            <div id="newAlbumForm"></div>

                            <div id="newRokAlbumForm"></div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div id="buttonBackUploadDone" style="margin-bottom: 1.5rem;display:none">
                            <button class="btn btn-submit" onclick="location.href = `uploadImagesForm`;">Zpět na formulář pro nahrávání</button>
                        </div>
                        <div class="row">
                            <div class="col-10" id="uploadLocationText"></div>
                            <div class="col-2" id="uploadedFilesCounter" style="text-align:right"></div>
                        </div>

                        <div id="uploadCompletedAlert"></div>

                        <div id="uploadProgressBar" style="display:none;margin-bottom:1rem">
                            <div class="progress" style="height: 20px;">
                                <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>

                        <table class="table table-striped" id="uploadingImagesTable" style="display:none">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Název souboru</th>
                                    <th>Velikost souboru</th>
                                </tr>
                            </thead>
                            <tbody id="thumbnailsArea"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../view/indexFooter.php") // Footer ?>
    </div>
  </body>
<script>
var pocetSouboru = 0;

function getFileName(){
    // Input file Rok
    const fileRok = document.querySelector('#fileRok');
    if (fileRok){
        fileRok.addEventListener('change', (e) => {
            const [fileRok] = e.target.files;
            const { name: fileNameRok } = fileRok;
            const fileNameAndSizeRok = `${fileNameRok} `;
            document.querySelector('.file-name-rok').textContent = fileNameAndSizeRok;
        });
    }
    
    // Input File Album
    const fileAlbum = document.querySelector('#fileAlbum');
    if(fileAlbum){
        fileAlbum.addEventListener('change', (e) => {
            const [fileAlbum] = e.target.files;
            const { name: fileNameAlbum } = fileAlbum;
            const fileNameAndSizeAlbum = `${fileNameAlbum} `;
            document.querySelector('.file-name-album').textContent = fileNameAndSizeAlbum;
        });
    }
    
    // Input file Image
    const fileImages = document.querySelector('#fileImages');
    if(fileImages){
        fileImages.addEventListener('change', function() {
            var numFiles = $(this).get(0).files.length
            const filesCount = numFiles+" soubory";
            document.querySelector('.file-name-images').textContent = filesCount;
        });
    }
}  
    
$("#newAlbumFormButton").click(function(){
    load_folder_list();

    function load_folder_list(){
    var action = "fetchRok";
    $.ajax({
        url : "../API/action.php",
        method : "POST",
        headers : {credentials: 'same-origin'},
        data : {action:action},
        success:function(data){
        $('#dropdown_rok').html(data);
        }
    })
    }
});

$('#newImageFormButton').click(function(){
    load_folder_list();

    function load_folder_list(){
    var action = "fetch";
    $.ajax({
        url : "../API/action.php",
        method : "POST",
        headers : {credentials: 'same-origin'},
        data : {action:action},
        success:function(data){
            $('#dropdown_rok').html(data);
        }
    })
    }
    $('body').on('change', '#album_year', function() {
    $(".album_option").each(function(){
        $(this).remove();
    });
    $("<option class='album_option'>Načítání...</option>").appendTo("#album_album");
    $.ajax({
        url : "../API/action.php",
        method : "POST",
        headers : {credentials: 'same-origin'},
        data : {rok:$(this).val()},
        success:function(data){
            var json = JSON.parse(data);
            $(".album_option").each(function(){
                $(this).remove();
            });
            json.forEach(function(item){
                $("<option class='album_option' value='"+item+"'>"+item+"</option>").appendTo("#album_album");
            });
        }
    })
    });
});

function getNewRokAlbumForm(){
    document.getElementById("newRokAlbumForm").innerHTML = `
    <input type="text" name="rok" class="uploadImageFormTextInput" placeholder="Rok" required autocomplete="off">
    <p class="uploadImageFormText">Náhledový obrázek roku:</p>
    <div class="file-input">   
        <input type="file" id="fileRok" class="file" name="thumbnailRok" required>
        <label for="fileRok">Vybrat soubor</label>
        <p class="file-name-rok file-name"></p> 
    </div>
    <input type="text" name="album" class="uploadImageFormTextInput" placeholder="Název Alba" required autocomplete="off">
    <p class="uploadImageFormText mt-4">Náhledový obrázek alba:</p>
    <div class="file-input mt-4">
        <input type="file" id="fileAlbum" class="file" name="thumbnailAlbum" required>
        <label for="fileAlbum">Vybrat soubor</label> 
        <p class="file-name-album file-name"></p>
    </div>
    <p style="width:100%;opacity:0;"> </p>
    <hr class="solid" style="width:95%">
    <p class="uploadImageFormText mt-4" >Obrázky do alba:</p>
    <div class="file-input mt-4">
        <input type="file" id="fileImages" class="file" name="files[]" multiple required>
        <label for="fileImages">Vybrat soubor</label> 
        <p class="file-name-images file-name"></p>
    </div>
    <button type="submit" class="btn uploadFormSubmitButton w-100 mt-3" name="submitRokAlbum">Nahrát 
        <i class="fas fa-cloud-upload-alt fa-2x" style="margin-left:0.5rem"></i>
    </button>`;
    document.getElementById("newAlbumForm").innerHTML = '';
    document.getElementById("newImageForm").innerHTML = '';
    getFileName();
}

function getNewAlbumForm(){
    document.getElementById("newAlbumForm").innerHTML = `
        <div id="dropdown_rok"></div>
        <input type="text" name="album" class="uploadImageFormTextInput" placeholder="Název Alba" required autocomplete="off">
        <p class="uploadImageFormText mt-4">Náhledový obrázek alba:</p>
        <div class="file-input mt-4">
            <input type="file" id="fileAlbum" class="file" name="thumbnailAlbum" required>
            <label for="fileAlbum">Vybrat soubor</label> 
            <p class="file-name-album file-name"></p>
        </div>
        <p style="width:100%;opacity:0;"> </p>
        <hr class="solid" style="width:95%">
        <p class="uploadImageFormText mt-4" >Obrázky do alba:</p>
        <div class="file-input mt-4">
            <input type="file" id="fileImages" class="file" name="files[]" multiple required>
            <label for="fileImages">Vybrat soubor</label> 
            <p class="file-name-images file-name"></p>
        </div>
        <button type="submit" class="btn uploadFormSubmitButton w-100 mt-3" name="submitAlbum">Nahrát 
            <i class="fas fa-cloud-upload-alt fa-2x" style="margin-left:0.5rem"></i>
        </button>
    `;
    document.getElementById("newRokAlbumForm").innerHTML = '';
    document.getElementById("newImageForm").innerHTML = '';
    getFileName();
}

function getNewImageForm(){
    document.getElementById("newImageForm").innerHTML = `
        <div id="dropdown_rok"></div>
        <p class="uploadImageFormText mt-4" >Obrázky do alba:</p>
        <div class="file-input mt-4">
            <input type="file" id="fileImages" class="file" name="files[]" multiple required>
            <label for="fileImages">Vybrat soubor</label> 
            <p class="file-name-images file-name"></p>
        </div>
        <button type="submit" class="btn uploadFormSubmitButton w-100 mt-3" name="submitImage">Nahrát 
            <i class="fas fa-cloud-upload-alt fa-2x" style="margin-left:0.5rem"></i>
        </button>
    `;
    document.getElementById("newRokAlbumForm").innerHTML = '';
    document.getElementById("newAlbumForm").innerHTML = '';
    getFileName();
}

$('body').on('click', 'button[name="submitRokAlbum"]', function(event){
    var valid = this.form.checkValidity();
    if (valid){
        uploadButtonClick(event,true,true);
    }
});
$('body').on('click', 'button[name="submitAlbum"]', function(event){
    var valid = this.form.checkValidity();
    if (valid){
        uploadButtonClick(event,true,false);
    }
});
$('body').on('click', 'button[name="submitImage"]', function(event){
    var valid = this.form.checkValidity();
    if (valid){
        uploadButtonClick(event);
    }
});

function humanFileSize(size) {
    var i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(1) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

function addRowToImagesTable(file) {
    var div = document.getElementById("thumbnailsArea");
    div.innerHTML += `
    <tr>
        <td style="padding-top: 0.8rem;padding-bottom: 0.8rem;">uploading</td>
        <td>`+file.name+`</td>
        <td>`+humanFileSize(file.size)+`</td>
    </tr> 
    `;
} 

function addUploadingImageRow(image){
    addRowToImagesTable(image);
}

function uploadButtonClick(event,thumbnailAlbum = false,thumbnailRok = false) {
    $("#typeOfUploadButtons").hide();
    $("#uploadImagesForm").hide();
    $("#uploadingImagesTable").show();
    $("#uploadProgressBar").show();

    event.preventDefault();
    var files = $('input[name="files[]"]')[0].files;
    pocetSouboru = files.length;

    files.forEach(addUploadingImageRow);
    
    var fd = new FormData();
    if(thumbnailAlbum){
        fd.append("thumbnailAlbum",$('input[name="thumbnailAlbum"]')[0].files[0]);
    }
    if(thumbnailRok){
        fd.append("thumbnailRok",$('input[name="thumbnailRok"]')[0].files[0]);
    }   
    
    fd.append(event.target.name,true);
    if($("input[name='rok']").length != 0 && $("input[name='album']").length != 0){
        var rok = $("input[name='rok']").val();
        var album = $("input[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
        $("#uploadLocationText").html("<p>Nahrávání do alba "+album+" v roce "+rok+"</p>");
    }
    if($("select[name='rok']").length != 0 && $("select[name='album']").length != 0){
        var rok = $("select[name='rok']").val();
        var album = $("select[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
        $("#uploadLocationText").html("<p>Nahrávání do alba "+album+" v roce "+rok+"</p>");
    }
    if($("select[name='rok']").length != 0 && $("input[name='album']").length != 0){
        var rok = $("select[name='rok']").val();
        var album = $("input[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
        $("#uploadLocationText").html("<p>Nahrávání do alba "+album+" v roce "+rok+"</p>");
    }

    //Nahrávání případných thumbnailů alb a roků
    $.ajax({
        url: '../API/uploadImages.php',
        type: 'POST',
        headers : {credentials: 'same-origin'},
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
            uploadImage(files,0)
        }
    }); 
}

async function uploadImage(images,i){
    var fd = new FormData();
    fd.append("files[]",images[i]);
    fd.append("submitRokAlbum",true);

    if($("input[name='rok']").length != 0 && $("input[name='album']").length != 0){
        var rok = $("input[name='rok']").val();
        var album = $("input[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
    }
    if($("select[name='rok']").length != 0 && $("select[name='album']").length != 0){
        var rok = $("select[name='rok']").val();
        var album = $("select[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
    }
    if($("select[name='rok']").length != 0 && $("input[name='album']").length != 0){
        var rok = $("select[name='rok']").val();
        var album = $("input[name='album']").val();
        fd.append("rok",rok);
        fd.append("album",album);
    }

    var ajax = $.ajax({
        url: '../API/uploadImages.php',
        type: 'POST',
        headers : {credentials: 'same-origin'},
        data: fd,
        contentType: false,
        processData: false,
    });
    ajax.done(function(response){
        const resp_json = JSON.parse(response);
        if(resp_json.ok){
            $("#uploadingImagesTable").children().eq(1).children().eq(i).children().first().html('<i class="fas fa-check-circle" style="font-size: 1.8rem;color: #707070;vertical-align: middle;margin-left: 0.3rem;"></i>');
        }else{
            $("#uploadingImagesTable").children().eq(1).children().eq(i).children().first().html('<i class="fas fa-times-circle" style="font-size: 1.8rem;color: #707070;vertical-align: middle;margin-left: 0.3rem;"></i>');
        }
        
        document.getElementById("uploadedFilesCounter").innerHTML = (i+1)+' / '+ pocetSouboru;

        var progressBar = $('.progress-bar');
        aktualniProgres = (i + 1)/pocetSouboru;
        if(aktualniProgres<=1){
            progressBar.css('width',aktualniProgres*100 + '%');
            progressBar.attr('aria-valuenow',aktualniProgres*100);
        }

        if(i+1 < images.length){
            uploadImage(images,i+1);
        }

        if(i==images.length-1){
            $("#buttonBackUploadDone").show();
            $("#uploadProgressBar").hide();
            $("#uploadLocationText").hide();
            $("#uploadedFilesCounter").hide();
            document.getElementById("uploadCompletedAlert").innerHTML += '<div class="alert" style="padding: 20px;background-color: #28a745;color: white;">Nahrávání do alba '+album+' v roce '+rok+' bylo dokončeno</div>';
        }
    });
}
</script>
</html>