<?php 
include ("config.php");
include ("../imports/client.php");
include ("../API/getArticle.php");
include ("../API/getSchuzky.php");
include ("../API/getUsers.php");
adminKick();
?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("../imports/head.php") ?>
    <title>Skaut Domašov</title>
  </head>
  <body>
    <?php include("../imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("../view/indexHeader.php");?>
            <div class="page_container">
            <div class="row">
                <div class="col-md-8 ">
                    <div class="blurBackground content_rozlozeni" >
                        <div style="width:95%;margin:auto">

                            <div id="disable_articles" style="display:none">
                                <div id="showEnableButton">
                                    <p style="text-align: left;margin-bottom:0"  >
                                        <i onclick="showEnableArticles()" class="fas fa-arrow-left iconAdd" style="cursor:pointer"></i>
                                        <span onclick="showEnableArticles()" style="cursor:pointer;font-size:1.4rem">Zpět na hlavní stránku<span>
                                    </p>
                                </div>
                                
                                <?php
                                    foreach ($articles_arr as &$article){
                                        if ( $article["enable"] == 0 ) {
                                            echo '
                                            <div class="row" style="position:relative">
                                                <a href="#" onclick="showDisabledEditor('.$article['id'].')" id="iconEdit?'.$article['id'].'">
                                                    <i class="far fa-edit iconEdit" style="margin-top: 1.5rem;"></i>
                                                </a>
                                                <form id="up_down_form" autocomplete="off" action="../API/updateArticle.php" method="POST">
                                                    <div class="form-group ">
                                                        <input type="hidden" name="aid" value="'.$article['id'].'">
                                                    </div>
                                                    <div class="form-group ">
                                                        <input type="hidden" name="poradi" value="'.$article['poradi'].'">
                                                    </div>
                                                </form>
                                                    <div style="display:none" id="editArticle?'.$article['id'].'" id="editor">
                                                        <form autocomplete="off" action="../API/updateArticle.php" method="POST">
                                                            <div class="form-group ">
                                                                <input type="hidden" name="aid" value="'.$article['id'].'">
                                                            </div>
                                                            <div class="form-group ">
                                                                <input type="hidden" name="poradi" value="'.$article['poradi'].'">
                                                            </div>
                                                            <div class="form-group ">
                                                                <input type="text" class="titleInput" value="'. $article['title'] .'" id="title" name="title" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="editor1" class="ckeditor" id="ckeditor">'. $article['description'] .'</textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="submit" class="btn btn-success" value="Upravit příspěvěk" id="save" name="save">
                                                                <input type="submit" class="btn btn-warning" value="Vyjmout příspěvek z archivu" name="odArchivovat">
                                                                <input type="button" class="btn btn-danger" value="Zrušit" onclick="removeDisabledEditor('.$article['id'].')">
                                                                <input type="submit" class="btn btn-danger" value="Odstranit příspěvěk" name="delete">
                                                            </div> 
                                                        </form>
                                                    </div>
                                                <p class="newsHeader" style="word-wrap: break-word;text-align:left" id="titleUpdate?'.$article['id'].'">'.$article["title"].'</p>
                                            </div>
                                            ';
                                        }
                                    }
                                ?>
                            </div>

                            <div id="enable_articles" style="display:block">
                                <div id="showDisableButton">
                                    <p style="text-align: right;margin-bottom:0"  >
                                        <span onclick="showDisableArticles()" style="cursor:pointer;font-size:1.4rem">Zobrazit archivované aktuality<span>
                                    </p>
                                </div>
                                <div id="addArticleButton">
                                    <p style="text-align: right;margin-bottom:0"  >
                                        <i onclick="addArticleEditor()" class="far fa-plus-square iconAdd" style="cursor:pointer"></i>
                                        <span onclick="addArticleEditor()" style="cursor:pointer;font-size:1.4rem">Přidat aktualitu<span>
                                    </p>
                                </div>
                                
                                <div id="editor"></div>
                                
                                <?php
                                    foreach ($articles_arr as &$article){
                                        if ( $article["enable"] == 1 ) {
                                            echo '
                                            <div class="row" style="position:relative">
                                                <a href="javascript:void()" onclick="showEditor('.$article['id'].')" id="iconEdit?'.$article['id'].'">
                                                    <i class="far fa-edit iconEdit" style="margin-top: 1.5rem;"></i>
                                                </a>
                                                <form id="up_down_form" autocomplete="off" action="../API/updateArticle.php" method="POST">
                                                    <div class="form-group ">
                                                        <input type="hidden" name="aid" value="'.$article['id'].'">
                                                    </div>
                                                    <div class="form-group ">
                                                        <input type="hidden" name="poradi" value="'.$article['poradi'].'">
                                                    </div>
                                                    <div class="form-group "> 
                                                        <button type="submit" name="up" id="iconUp?'.$article['id'].'" class="fabutton iconUp">
                                                            <i class="fas fa-arrow-up "></i>
                                                        </button>
                                                        <button type="submit" name="down" id="iconDown?'.$article['id'].'" class="fabutton iconDown">
                                                            <i class="fas fa-arrow-down "></i>
                                                        </button>
                                                    </div>
                                                </form>
                                                    <div style="display:none" id="editArticle?'.$article['id'].'" id="editor">
                                                        <form autocomplete="off" action="../API/updateArticle.php" method="POST">
                                                            <div class="form-group ">
                                                                <input type="hidden" name="aid" value="'.$article['id'].'">
                                                            </div>
                                                            <div class="form-group ">
                                                                <input type="hidden" name="poradi" value="'.$article['poradi'].'">
                                                            </div>
                                                            <div class="form-group ">
                                                                <input type="text" class="titleInput" value="'. $article['title'] .'" id="title" name="title" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="editor1" class="ckeditor" id="ckeditor">'. $article['description'] .'</textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="submit" class="btn btn-success" value="Upravit příspěvěk" id="save" name="save">
                                                                <input type="submit" class="btn btn-warning" value="Archivovat příspěvěk" name="archivovat">
                                                                <input type="button" class="btn btn-danger" value="Zrušit" onclick="removeArticleEditor('.$article['id'].')">
                                                                <input type="submit" class="btn btn-danger" value="Odstranit příspěvěk" name="delete">
                                                            </div> 
                                                        </form>
                                                    </div>
                                                <p class="newsHeader" style="word-wrap: break-word;" id="titleUpdate?'.$article['id'].'">'.$article["title"].'</p>
                                                <div class="newsContent" style="word-wrap: break-word;" id="descriptionUpdate?'.$article['id'].'">'.$article["description"].'</div>
                                            </div>
                                            ';
                                        }
                                    }
                                ?>
                            </div>  
                        </div>
                    </div>   
                </div>
                
                <div class="col-md-4" >
                    
                    <div class="blurBackground schuzka_rozlozeni">
                        <div style="width:95%;margin:auto">
                            <?php 
                                echo "
                                    <div style='text-align:right' class='userInfo'>
                                        <p class='user_text_header' style='margin-bottom:0rem'>Přihlášen jako ".getFullName()."</p>";
                                        if(isRoot()){
                                            echo "
                                            <p class='user_text_header mb-0' data-mdb-toggle='modal' data-mdb-target='#modalSpravaUzivatelskychUctu'>
                                                <a href='#'>Správa uživatelských účtů</a>
                                            </p> ";
                                        }
                                        echo"
                                        <p class='user_text_header mb-0' data-mdb-toggle='modal' data-mdb-target='#modalVytvoreniUzivatele'>
                                            <a href='#'>Vytvořit nového uživatele</a>
                                        </p>
                                        <p class='user_text_header mb-0' data-mdb-toggle='modal' data-mdb-target='#modalZmenaHesla'>
                                            <a href='#' onclick='passwordResetModalClicked()'>Změnit heslo</a>
                                        </p> 
                                        <p class='user_text_header' style='margin-bottom:0.2rem'><a href='../client/logout.php'>Odhlásit se</a></p>
                                    </div>
                                ";
                            ?>
                        </div>
                    </div>
                    <?php
                    if(isRoot()){
                    echo'
                        <div class="modal fade" id="modalSpravaUzivatelskychUctu" tabindex="-1" aria-labelledby="modalSpravaUzivatelskychUctuLabel" aria-hidden="true">
                            <div class="modal-dialog" style="max-width:70% !important;margin: 1.75rem auto !important;">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalSpravaUzivatelskychUctuLabel">Správa uživatelských účtů</h5>
                                    <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div style="overflow-x:auto;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Uživateslé jméno</th>
                                                <th>E-mailová adresa</th>
                                                <th>Křestní jméno</th>
                                                <th>Příjmení</th>
                                                <th>Role</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                                
                                            
                                            
                                                foreach($users_arr as &$user){
                                                    echo '
                                                    <tr>
                                                        <td>'.$user["username"].'</td>
                                                        <td>'.$user["email"].'</td>
                                                        <td>'.$user["firstname"].'</td>
                                                        <td>'.$user["lastname"].'</td>
                                                        
                                                    ';
                                                    if($user["role"] == 0){
                                                        echo '<td>Uživatel</td>';
                                                    }elseif ($user["role"] == 1) {
                                                        echo '<td>Admin</td>';
                                                    }elseif ($user["role"] == 2) {
                                                        echo '<td>Root</td>';
                                                    }
                                                    echo '
                                                        <td>
                                                        <form action="../API/deleteUser.php" method="POST" id="deleteUser_'.$user["id"].'">
                                                            <input type="hidden" value="'.$user["id"].'" name="id">
                                                            <i class="far fa-trash-alt " style="cursor:pointer;font-size:1.3rem" data-mdb-toggle="modal" data-mdb-target="#modalPotvrditOdstraneni_'.$user["id"].'"></i>
                                                        

                                                            <div class="modal fade" id="modalPotvrditOdstraneni_'.$user["id"].'" tabindex="-1" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content" style="background-color: #efefef !important;margin-top: 5rem;border:1;">
                                                                        <div class="modal-body">
                                                                            <p>Opravdu si přejete uživatelský účet '.$user["firstname"].' '.$user["lastname"].' odstranit</p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                                                            <button type="submit" class="btn btn-danger" >Odstranit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 

                                                        </td>
                                                    </tr>';
                                                }          
                        echo '                 
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                </div>
                                </div>
                            </div>
                        </div> ';
                    }                      
                    ?>

                    <div class="modal fade" id="modalZmenaHesla" tabindex="-1" aria-labelledby="modalZmenaHeslaLabel" aria-hidden="true">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalZmenaHeslaLabel">Změna hesla</h5>
                                    <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form action="../API/resetPassword.php"  method="POST">
                                        <p class="registerForm" > <input class="registerInput" type="email" id="resetPasswordInput" name="email" placeholder="E-mail" required></p>
                                        <p style="text-align: center;margin-top:0.5rem;margin-bottom:0;">Po potvrzení vyčkejte na ověřovací email</p>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                    <button type="submit" name="odeslano" class="btn btn-success">Potvrdit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="modal fade" id="modalVytvoreniUzivatele" tabindex="-1" aria-labelledby="modalVytvoreniUzivateleLabel" aria-hidden="true">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalVytvoreniUzivateleLabel">Vytvoření uživatele</h5>
                                    <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form  method="POST" style="margin:auto" action="../API/createUser.php">
                                        <p class="registerForm">Uživatelské jméno:</p>
                                        <p class="registerForm" > <input class="registerInput" type="text" name="username" placeholder="" required></p>

                                        <p class="registerForm">E-mail:</p>
                                        <p class="registerForm" > <input class="registerInput" type="email" name="email" placeholder="" required></p>

                                        <p class="registerForm">Heslo:</p>
                                        <p class="registerForm"> <input class="registerInput" type="password" name="password" placeholder="" required></p>

                                        <p class="registerForm">Křestní jméno:</p>
                                        <p class="registerForm"> <input class="registerInput" type="text" name="firstname" placeholder="" required></p>

                                        <p class="registerForm">Přijmení:</p>
                                        <p class="registerForm"> <input class="registerInput" type="text" name="lastname" placeholder="" required></p>


                                        <?php 
                                        if(isRoot()){
                                            echo '<p class="registerForm">Admin: <input type="checkbox" name="role"></p>';
                                        }
                                        ?>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                    <button type="submit" name="odeslano" class="btn btn-success">Potvrdit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="blurBackground schuzka_rozlozeni"  style="position:relative">
                        <div style="width:95%;margin:auto">
                            <div class="schuzky">
                                <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">Schůzky</p>
                                <?php
                                    foreach($schuzky_arr as &$schuzka){
                                        echo '
                                        <div class="row schuzkarow" style="position:relative" >
                                            <div class="col-5 col-md-12 col-xl-5">
                                                <p class="schuzka_oddil schuzka_oddil_admin" id="oddil?'.$schuzka["id"].'">'.$schuzka["oddil"].'</p>
                                            </div>
                                            <div class="col-2 col-md-4 col-xl-2">
                                                <p class="schuzka_den schuzka_den_admin" id="den?'.$schuzka["id"].'">'.$schuzka["den"].'</p>
                                            </div>
                                            <div class="col-5 col-md-8 col-xl-5">
                                                <p class="schuzka_cas schuzka_cas_admin" style="margin-right:1.4rem" id="cas?'.$schuzka["id"].'">'.$schuzka["zacatek"].'-'.$schuzka["konec"].'</p>
                                                <a href="#"  onclick="showEditorSchuzky('.$schuzka['id'].')" id="iconEditSchuzka?'.$schuzka['id'].'">
                                                    <i class="far fa-edit iconEditSchuzky" style="margin-bottom:-4rem" ></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" id="editSchuzka?'.$schuzka['id'].'" id="editor">
                                            <form autocomplete="off" action="../API/updateSchuzky.php" method="POST">
                                                <div class="row">
                                                    <div class="form-group ">
                                                        <input type="hidden" name="sid" value="'.$schuzka['id'].'">
                                                    </div>
                                                    <div class="col-5" style="padding:0;padding-left:10px">
                                                        <div class="form-group ">
                                                            <input type="text" class="titleInput" style="text-align:left;font-size:1rem" value="'. $schuzka['oddil'] .'" id="oddil" name="oddil" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-2" style="padding:0">
                                                        <div class="form-group ">
                                                            <input style="font-size:1rem" type="text" class="titleInput" value="'. $schuzka['den'] .'" id="den" name="den" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-2" style="padding:0">
                                                        <div class="form-group ">
                                                            <input style="font-size:1rem" type="text" class="titleInput" value="'. $schuzka['zacatek'] .'" id="zacatek" name="zacatek" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-3" style="padding:0;padding-right:10px">
                                                        <div class="form-group ">
                                                            <input style="font-size:1rem" type="text" class="titleInput" value="'. $schuzka['konec'] .'" id="konec" name="konec" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn-sm btn-success" value="Uložit" id="save" name="save">
                                                    <input type="button" class="btn-sm btn-danger" value="Zrušit" onclick="removeEditorSchuzky('.$schuzka['id'].')">
                                                </div> 
                                            </form>
                                        </div>
                                        ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="blurBackground schuzka_rozlozeni">
                        <div style="width:95%;margin:auto">
                            <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">
                                Naši činnost podpořil
                            </p>
                            <div class="text-center">
                                <img src="../public/img/jihomoravsky_kraj.png" width="80%" alt="" class="rounded" style="margin-bottom:1rem">
                            </div>
                        </div>
                    </div>

                    <div class="blurBackground schuzka_rozlozeni" >
                        <div style="width:95%;margin:auto">
                            <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">Kalendář akcí</p> 
                            <div>
                                <iframe src="https://calendar.google.com/calendar/embed?height=400&wkst=2&bgcolor=%23ffffff&ctz=Europe%2FPrague&src=OW5qdXI4YjYzbGUwNmt0dXBtZ2ZrcXJwOW9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&src=Y3MuY3plY2gjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&color=%23EF6C00&color=%230B8043&showNav=1&showTitle=0&showCalendars=0&showTabs=1&showPrint=0&showTz=0&showDate=1&mode=AGENDA" style="border-width:0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            </div>
        </div>
        <?php include("../view/indexFooter.php") ?>
    </div>
  </body>
<script>
    function addArticleEditor() {
        const div = document.createElement('div');
        div.className = 'addArticle';
        div.innerHTML = `
            <form autocomplete="off" action="../API/createArticle.php" method="POST" role="form">
                <input type="hidden" name="aid" id="aid">
                <div class="form-group ">
                    <input type="text" class="titleInput" placeholder="Nadpis" id="title" name="title" required>
                </div>
                <div class="form-group">
                    <textarea name="editorCreate"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Vytvořit příspěvek" id="save" name="save">
                    <input type="button" class="btn btn-danger" value="Zrušit" onclick="removeArticleMaker()">
                </div>
            </form>
        `;
        document.getElementById('editor').appendChild(div);
        CKEDITOR.replace( 'editorCreate' );
        const addButton = document.getElementById("addArticleButton");
        const showButton = document.getElementById("showDisableButton");
        addButton.innerHTML = '';
        showButton.innerHTML = '';
    }

    function removeArticleMaker() {
        const ckEditor = document.getElementById("editor");
        ckEditor.innerHTML = '';
        const addButton = document.getElementById("addArticleButton");
        addButton.innerHTML = '<p style="text-align: right;margin-bottom:0"  ><i onclick="addArticleEditor()" class="far fa-plus-square iconAdd" style="cursor:pointer"></i><span onclick="addArticleEditor()" style="cursor:pointer;font-size:1.4rem">Přidat aktualitu<span></p>';
        const showButton = document.getElementById("showDisableButton");
        showButton.innerHTML = '<p style="text-align: right;margin-bottom:0" onclick="showDisableArticles()" ><span style="cursor:pointer;font-size:1.25rem"> Zobrazit archivované aktuality<span></p>';
    }

    function showEditor(id){
        document.getElementById("editArticle?"+id).style.display = 'block';
        document.getElementById("iconEdit?"+id).style.display = 'none';
        document.getElementById("iconUp?"+id).style.display = 'none';
        document.getElementById("iconDown?"+id).style.display = 'none';
        document.getElementById("titleUpdate?"+id).style.display = 'none';
        document.getElementById("descriptionUpdate?"+id).style.display = 'none';
    }

    function removeArticleEditor(id){
        document.getElementById("editArticle?"+id).style.display = 'none';
        document.getElementById("iconEdit?"+id).style.display = 'block';
        document.getElementById("iconUp?"+id).style.display = 'block';
        document.getElementById("iconDown?"+id).style.display = 'block';
        document.getElementById("titleUpdate?"+id).style.display = 'block';
        document.getElementById("descriptionUpdate?"+id).style.display = 'block';
    }

    function showDisabledEditor(id){
        document.getElementById("editArticle?"+id).style.display = 'block';
        document.getElementById("iconEdit?"+id).style.display = 'none';
        document.getElementById("titleUpdate?"+id).style.display = 'none';
        document.getElementById("descriptionUpdate?"+id).style.display = 'none';
    }

    function removeDisabledEditor(id){
        document.getElementById("editArticle?"+id).style.display = 'none';
        document.getElementById("iconEdit?"+id).style.display = 'block';
        document.getElementById("titleUpdate?"+id).style.display = 'block';
        document.getElementById("descriptionUpdate?"+id).style.display = 'block';
    }
    
    function deleteArticle(){
        document.getGetElementById("deleteForm").submit();
    }

    function showEditorSchuzky(id){
        document.getElementById("editSchuzka?"+id).style.display = 'block';
        document.getElementById("iconEditSchuzka?"+id).style.display = 'none';
        document.getElementById("oddil?"+id).style.display = 'none';
        document.getElementById("den?"+id).style.display = 'none';
        document.getElementById("cas?"+id).style.display = 'none';
    }

    function removeEditorSchuzky(id){
        document.getElementById("editSchuzka?"+id).style.display = 'none';
        document.getElementById("iconEditSchuzka?"+id).style.display = 'block';
        document.getElementById("oddil?"+id).style.display = 'block';
        document.getElementById("den?"+id).style.display = 'block';
        document.getElementById("cas?"+id).style.display = 'block';
    }
    function showDisableArticles(){
        document.getElementById("disable_articles").style.display = 'block';
        document.getElementById("enable_articles").style.display = 'none';
    }
    function showEnableArticles(){
        document.getElementById("enable_articles").style.display = 'block';
        document.getElementById("disable_articles").style.display = 'none';
    }

    function passwordResetModalClicked(){
        document.getElementById("resetPasswordInput").value = "<?php echo getUserEmail()?>";
    }   

    window.onload = function() {
        var imgs = document.querySelectorAll('div.content_rozlozeni img');
        [].forEach.call(imgs, function(element, index, array) {
            element.classList.add("img-fluid");
        });
        $(".img-fluid").each(function(){
        var width = $(this).parent().width();
        var img_width = $(this).width();
        var rel_width = img_width / width;
        $(this).css("width",rel_width*100+"%");
    });
    }   
</script>
</html>
