<?php 
include ("../imports/client.php");
adminKick();
?>

<!DOCTYPE html>
<html lang="cs">
    <head>
        <title>Skaut Domašov | Galerie</title>
        <link href="../lightGallery/css/lightgallery.css" rel="stylesheet"> 
        <?php include("../imports/head.php") ?>
        <style>
            .lg-backdrop {
                background-color: rgba(255, 255, 255, 0.8);
            }
            .lg-icon{
                color: rgb(45, 45, 45) !important;
            }
            #lg-counter{
                color: rgb(45, 45, 45) !important;
            }
        </style>
    </head>
    <body>
        <?php include("../imports/body.php") ?>
        <div class="page">
            <div class="container-fluid" >
            <?php include("../view/indexHeader.php");?>
                <div class="page_container">
                    <p id="btnBack" style="text-align: left;margin-bottom:15px">
                        <i onclick="back();" class="fas fa-arrow-left iconAdd" style="cursor:pointer"></i>
                        <span onclick="back();" style="cursor:pointer;font-size:1.4rem">Zpět<span>
                    </p> 
                    
                    <div class="row" id="gallery_albums_holder"></div>
                    <div class="row" id="lightgallery"></div>
                    <div class="row" id="nextPage" style="display:none">
                        <div onclick="nextPage()" style="margin: auto;text-align: center;font-size: 1.6rem;cursor:pointer;width:10%;">
                            <i class="fas fa-angle-double-down" style="margin-bottom: 1.5rem"></i>
                        </div>
                    </div>
                </div>
            </div>  
            <?php include("../view/indexFooter.php")?>
        </div>
    </body>
<script src="../lightGallery/js/lightgallery.js"></script> 
<script src="../lightGallery/js/lg-rotate.js"></script>    
<script src="../lightGallery/js/lg-fullscreen.js"></script>
<script src="../lightGallery/js/lg-zoom.js"></script>
<script src="../lightGallery/js/lg-autoplay.js"></script>
<script>
var currentPage = 0;
var currentYear = null;
var currentAlbum = null;

let yearsArray = [];
let albumsArray = [];

function back(){
    if(currentYear != null && currentAlbum == null){
        //Back to years
        years();
        currentAlbum = null;
        currentYear = null;
    }
    if(currentYear != null && currentAlbum != null){
        //Back to albums
        onYearClicked(currentYear);
        document.getElementById("nextPage").style.display = "none";
        currentAlbum = null;
    }
}

function nextPage(){
    currentPage++;
    onAlbumClicked(currentAlbum,currentPage);
}

function onAlbumClicked(item,page){
    if(page == 0){
        $("#gallery_albums_holder").children().each(function(){$(this).remove()});
        $("#lightgallery").children().each(function(){$(this).remove()});
    }
    $.ajax({
        url : "../API/getImages.php",
        method : "POST",
        headers : {credentials: 'same-origin'},
        data : {year:currentYear, album:item, page:page},
        success:function(data){
            var json = JSON.parse(data);
            if(json.count-page*20>20){
                $("#nextPage").show();
            }else{
                $("#nextPage").hide();
            }
            currentAlbum = item;
            json.images.forEach(function(item){
                $(`
                <div class="col-6 col-sm-4 col-md-3" data-src="../gallery_images/`+item.rok+`/`+item.album+`/`+item.name+`.`+item.filetype+`">
                    <a href="">
                        <img class="galerieImage" src="../gallery_images/`+item.rok+`/`+item.album+`/thumbnail/`+item.name+`.`+item.filetype+`" />
                    </a>
                </div>
                `).appendTo("#lightgallery");
            });
            if(json.length == 0){
                $('<p>Žádné obrázky</p>').appendTo("#gallery_albums_holder");
            }
            console.log(json);
            lightGallery(document.getElementById('lightgallery')); 
        }
    })
}


function onYearClicked(item){
    document.getElementById("btnBack").style.display="block";
    $("#gallery_albums_holder").children().each(function(){$(this).remove()});
    $("#lightgallery").children().each(function(){$(this).remove()});

    $.ajax({
        url : "../API/getAlbums.php",
        method : "POST",
        headers : {credentials: 'same-origin'},
        data : {year:String(item)},
        success:function(data){
            var json = JSON.parse(data);
            currentYear = item;
            albumsArray = [];
            json.forEach(function(item){
                albumsArray.push(item.album);
                $(`
                <div class="col-6 col-sm-4 col-md-3">
                    <div class="modal fade" id="exampleModal_id_`+String(item.id)+`" tabindex="-1" aria-labelledby="exampleModalLabel_id_`+String(item.rok)+`" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel_id_`+String(item.id)+`">Úprava alba `+String(item.album)+`</h5>
                                    <button type="button" class="btn-close" onclick="hideModalForms(`+String(item.rok)+`)" data-mdb-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <span id="selectForm_id_`+String(item.id)+`">Vyberte akci</span>
                                    <div id="updateThumbnailForm_id_`+String(item.id)+`" style="display:none;">
                                        <form method="POST" action="../API/updateThumbnail.php" enctype="multipart/form-data">
                                            <input type="hidden" name="path" value="`+String(item.path)+`">
                                            <input type="hidden" name="album" value="`+String(item.album)+`">
                                            <label class="form-label" for="rokThumbnailInput">Vyberte náhledový obrázek</label>
                                            <input type="file" name="thumbnailAlbum" required class="form-control" id="rokThumbnailInput" />
                                            <button type="submit" style="margin-top: 1.5rem;" class="btn btn-success" name="submitAlbum">Uložit</button>
                                        </form>
                                    </div>
                                    <div id="deleteAlbumForm_id_`+String(item.id)+`" style="display:none;">
                                        <p>
                                            Opravdu chcete album odstranit
                                            <form method="POST" action="../API/deleteGallery.php" enctype="multipart/form-data">
                                                <input type="hidden" name="albumId" value="`+String(item.id)+`">
                                                <input type="hidden" name="path" value="`+String(item.path)+`">
                                                <input type="hidden" name="album" value="`+String(item.album)+`">
                                                <button type="submit" class="btn btn-danger" name="deleteAlbum">Odstranit</button>
                                            </form>
                                        </p>
                                    </div>
                                    <div id="renameAlbumForm_id_`+String(item.id)+`" style="display:none;">
                                        <p>
                                            <form method="POST" action="../API/updateThumbnail.php" enctype="multipart/form-data">
                                                <input type="hidden" name="path" value="`+String(item.path)+`">
                                                <input type="hidden" name="album" value="`+String(item.album)+`">
                                                <input type="text" name="albumName" required id="rename_id_`+String(item.id)+`" class="form-control" autocomplete="off" placeholder="Název Alba"/> 
                                                <span id="renameZprava_id_`+String(item.id)+`" style="position:absolute"></span>
                                                <button type="submit" style="margin-top: 2rem;" id="renameSubmit_id_`+String(item.id)+`" class="btn btn-success" name="submitAlbumName">Uložit</button>
                                            </form>
                                        </p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" onclick="showDeleteAlbumForm('id_`+String(item.id)+`')" class="btn btn-danger">Odstranit Album</button>
                                    <button type="button" onclick="showUpdateThumbnailForm('id_`+String(item.id)+`')" class="btn btn-primary">Změnit thumbnail</button>
                                    <button type="button" onclick="showUpdateNameForm('id_`+String(item.id)+`')" class="btn btn-primary">Změnit Název</button>
                                    <button type="button" onclick="hideModalForms('id_`+String(item.id)+`')" style="color: darkgrey;border-color: darkgrey;margin-right:auto" class="btn btn-outline-light" data-mdb-dismiss="modal">Zrušit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" onclick="onAlbumClicked('`+item.album+`',0);">
                        <img src="`+item.path+`"   class="img-thumbnail galerieAlbumRok" alt=""> 
                    </a>
                    <p style="text-align:center;margin-bottom:2rem;color:#4a4a4a;position:relative">`+item.album+`
                        <i class="far fa-edit editThumbnail"  data-mdb-toggle="modal" data-mdb-target="#exampleModal_id_`+String(item.id)+`"></i>
                    </p>
                </div>
                `).appendTo("#gallery_albums_holder");
            });
            if(json.length == 0){
                $('<p>Žádná alba</p>').appendTo("#gallery_albums_holder");
            }
        }
    })
}

function showUpdateThumbnailForm(name){
document.getElementById("updateThumbnailForm_"+name).style.display="block";
document.getElementById("deleteAlbumForm_"+name).style.display="none";
document.getElementById("selectForm_"+name).style.display="none";
document.getElementById("renameAlbumForm_"+name).style.display="none";
}

let renameAlbumRokId = "";

function showUpdateNameForm(name){
    if ($('#renameAlbumRok').length > 0) {
        document.getElementById("renameAlbumRok").id = "rename_" + renameALbumRokId;
    }
    if ($('#renameAlbumRokSubmit').length > 0) {
        document.getElementById("renameAlbumRokSubmit").id = "renameSubmit_" + renameALbumRokId;
    }
    if ($('#renameZprava').length > 0) {
        document.getElementById("renameZprava").id = "renameZprava_" + renameALbumRokId;
    }
    
    renameALbumRokId = name;

    document.getElementById("renameAlbumForm_"+name).style.display="block";
    document.getElementById("updateThumbnailForm_"+name).style.display="none";
    document.getElementById("deleteAlbumForm_"+name).style.display="none";
    document.getElementById("selectForm_"+name).style.display="none";

    document.getElementById("rename_"+name).id = "renameAlbumRok";
    document.getElementById("renameSubmit_"+name).id = "renameAlbumRokSubmit";
    document.getElementById("renameZprava_"+name).id = "renameZprava";
}

function showDeleteAlbumForm(name){
    document.getElementById("updateThumbnailForm_"+name).style.display="none";
    document.getElementById("deleteAlbumForm_"+name).style.display="block";
    document.getElementById("selectForm_"+name).style.display="none";
    document.getElementById("renameAlbumForm_"+name).style.display="none";
}
function hideModalForms(name){
    document.getElementById("updateThumbnailForm_"+name).style.display="none";
    document.getElementById("deleteAlbumForm_"+name).style.display="none";
    document.getElementById("renameAlbumForm_"+name).style.display="none";
    document.getElementById("selectForm_"+name).style.display="block";
}

$('#gallery_albums_holder').on('keyup', "#renameAlbumRok" , function () {
    if (yearsArray.includes($('#renameAlbumRok').val())) {
        console.log("Tento rok již existuje");
        document.getElementById("renameAlbumRokSubmit").setAttribute('disabled','disabled');
        $('#renameZprava').html('Tento rok již existuje').css('color', 'red');
    } else if (albumsArray.includes($('#renameAlbumRok').val())) {
        console.log("Toto album již existuje");
        document.getElementById("renameAlbumRokSubmit").setAttribute('disabled','disabled');
        $('#renameZprava').html('Toto album již existuje').css('color', 'red');
    }else {
        console.log("Může být");
        document.getElementById("renameAlbumRokSubmit").removeAttribute('disabled','disabled');
        $('#renameZprava').html('').css('color', 'red');
    }
});

function years(){
    document.getElementById("btnBack").style.display="none";
    $("#gallery_albums_holder").children().each(function(){$(this).remove()});
    $.ajax({
    url : "../API/getGallery.php",
    method : "POST",
    headers : {credentials: 'same-origin'},
    data : {},
    success:function(data){
        var json = JSON.parse(data);
        json.forEach(function(item){
            yearsArray.push(item.rok);
            $(`
            <div class="col-6 col-sm-4 col-md-3">
                <div class="modal fade" id="exampleModal_`+String(item.rok)+`" tabindex="-1" aria-labelledby="exampleModalLabel_`+String(item.rok)+`" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel_`+String(item.rok)+`">Úprava roku `+String(item.rok)+`</h5>
                                <button type="button" class="btn-close" onclick="hideModalForms(`+String(item.rok)+`)" data-mdb-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <span id="selectForm_`+String(item.rok)+`">Vyberte akci</span>
                                <div id="updateThumbnailForm_`+String(item.rok)+`" style="display:none;">
                                    <form method="POST" action="../API/updateThumbnail.php" enctype="multipart/form-data">
                                        <input type="hidden" name="rok" value="`+String(item.rok)+`">
                                        <label class="form-label" for="rokThumbnailInput">Vyberte náhledový obrázek</label>
                                        <input type="file" name="thumbnailRok" required class="form-control" id="rokThumbnailInput" />
                                        <button type="submit" style="margin-top: 1.5rem;" class="btn btn-success" name="submitRok">Uložit</button>
                                    </form>
                                </div>
                                <div id="deleteAlbumForm_`+String(item.rok)+`" style="display:none;">
                                    <p> 
                                        Opravdu chcete rok i jeho alba odstranit
                                        <form method="POST" action="../API/deleteGallery.php" enctype="multipart/form-data">
                                            <input type="hidden" name="rok" value="`+String(item.rok)+`">
                                            <input type="hidden" name="path" value="`+String(item.path)+`">
                                            <button type="submit" class="btn btn-danger" name="deleteRok">Odstranit</button>
                                        </form>
                                    </p>
                                </div>
                                <div id="renameAlbumForm_`+String(item.rok)+`" style="display:none;">
                                    <p>
                                        <form method="POST" action="../API/updateThumbnail.php" enctype="multipart/form-data">
                                            <input type="hidden" name="path" value="`+String(item.path)+`">
                                            <input type="hidden" name="rok" value="`+String(item.rok)+`">
                                            <input type="text" name="rokName" required id="rename_`+String(item.rok)+`" class="form-control " autocomplete="off" placeholder="Název Roku"/> 
                                            <span id="renameZprava_`+String(item.rok)+`" style="position:absolute"></span>
                                            <button type="submit" style="margin-top: 2rem;" id="renameSubmit_`+String(item.rok)+`" class="btn btn-success" name="submitRokName">Uložit</button>
                                        </form>
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="showDeleteAlbumForm(`+String(item.rok)+`)" class="btn btn-primary">Odstranit Rok</button>
                                <button type="button" onclick="showUpdateThumbnailForm(`+String(item.rok)+`)" class="btn btn-primary">Změnit thumbnail</button>
                                <button type="button" onclick="showUpdateNameForm(`+String(item.rok)+`)" class="btn btn-primary">Změnit Název</button>
                                <button type="button" onclick="hideModalForms(`+String(item.rok)+`)" style="background-color:darkgrey;margin-right:auto" class="btn btn-light" data-mdb-dismiss="modal">Zrušit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#" style="position:relative" onclick="onYearClicked('`+String(item.rok)+`');">
                    <img src="`+item.path+`"   class="img-thumbnail galerieAlbumRok" alt="" >
                </a>
                <p style="text-align:center;margin-bottom:2rem;color:#4a4a4a;position:relative">`+item.rok+` 
                    <i class="far fa-edit editThumbnail"  data-mdb-toggle="modal" data-mdb-target="#exampleModal_`+String(item.rok)+`"></i>
                </p>  
            </div>
            `).appendTo("#gallery_albums_holder");
        });
        if(json.length == 0){
            $('<p>Galerie je prázdná</p>').appendTo("#gallery_albums_holder");
        }
    }
    })
}
years();
</script>
</html>