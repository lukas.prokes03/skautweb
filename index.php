<?php 
include ("admin/config.php");
include ("imports/client.php");
include ("API/getArticle.php"); 
include ("API/getSchuzky.php");
?>

<!DOCTYPE html> 
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php")  ?>
            <div class="page_container">
              <div class="row">
                <div class="col-md-8 ">
                    <div class="blurBackground content_rozlozeni" >
                        <div style="width:95%;margin:auto">
                            <p style="text-align: right;margin-bottom:0" data-mdb-toggle="modal" data-mdb-target="#modalOdberNovinek" >
                                <span style="cursor:pointer;font-size:0.9rem">Přihlásit se k odběru novinek<span>
                            </p>
                            
                            <div class="modal fade" id="modalOdberNovinek" tabindex="-1" aria-labelledby="modalOdberNovinekLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modalOdberNovinekLabel">Odběr novinek</h5>
                                        <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="API/createSubscriber" method="POST">        
                                            <input type="email" class="form-control" name="email" placeholder="Váš e-mail" required autocomplete="off"/>
                                            
                                            <p style="margin-top:0.7rem;margin-bottom: 0;"><input type="checkbox" name="" required id="">  Souhlasím se zpracováním osobních údajů</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                        <button type="submit" name="submit" class="btn btn-success">Přihlásit se k odběru</button>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                                foreach ($articles_arr as &$article){
                                    if ( $article["enable"] == 1 ) {
                                        echo '
                                        <div class="row">
                                            <p class="newsHeader" style="word-wrap: break-word;">'.$article["title"].'</p>
                                            <div class="newsContent" style="word-wrap: break-word;">'.$article["description"].'</div>
                                        </div>
                                        ';
                                    }
                                }
                            ?>
                        </div>      
                    </div>   
                </div>
                <div class="col-md-4">   
                    <?php 
                        if (isLogged()){
                            echo "
                            <div class='blurBackground schuzka_rozlozeni'>
                                <div style='width:95%;margin:auto'>
                                    <div style='text-align:right' class='userInfo'>
                                        <p class='user_text_header' style='margin-bottom:0.2rem'>Přihlášen jako ".getFullName()."</p>";
                            if(isAdmin()){         
                                echo "<p class='user_text_header mb-0'><a href='admin/adminIndex.php'>Admin Panel</a></p>";
                            }
                            echo "
                                <p class='user_text_header mb-0' data-mdb-toggle='modal' data-mdb-target='#modalZmenaHesla'>
                                    <a href='#' onclick='passwordResetModalClicked()'>Změnit heslo</a>
                                </p> 
                                <p class='user_text_header' style='margin-bottom:0'><a href='client/logout.php'>Odhlásit se</a></p>
                            </div>
                            </div>
                            </div>";
                        }
                    ?>
                    
                    <div class="modal fade" id="modalZmenaHesla" tabindex="-1" aria-labelledby="modalZmenaHeslaLabel" aria-hidden="true">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalZmenaHeslaLabel">Změna hesla</h5>
                                    <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form action="../API/resetPassword.php"  method="POST">
                                        <p class="registerForm" > <input class="registerInput" type="email" name="email"  id="resetPasswordInput" placeholder="E-mail" required></p>
                                        <p style="text-align: center;margin-top:0.5rem;margin-bottom:0;">Po potvrzení vyčkejte na ověřovací email</p>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-mdb-dismiss="modal">Zrušit</button>
                                    <button type="submit" name="odeslano" class="btn btn-success">Potvrdit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="blurBackground schuzka_rozlozeni">
                        <div style="width:95%;margin:auto">
                            <div class="schuzky">
                                <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">Schůzky</p>
                                <?php
                                    foreach($schuzky_arr as &$schuzka){
                                        echo '
                                        <div class="row schuzkarow" >
                                            <div class="col-5 col-md-12 col-xl-5">
                                              <p class="schuzka_oddil">'.$schuzka["oddil"].'</p>
                                            </div>
                                            <div class="col-2 col-md-6 col-xl-2">
                                              <p class="schuzka_den schuzka_den_zkratka">'.$schuzka["den"].'</p>
                                              <p class="schuzka_den schuzka_den_cely">'.$schuzka["denCely"].'</p>
                                            </div>
                                            <div class="col-5 col-md-6 col-xl-5">
                                              <p class="schuzka_cas">'.$schuzka["zacatek"].'-'.$schuzka["konec"].'</p>
                                            </div>
                                        </div>
                                        ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="blurBackground schuzka_rozlozeni" >
                        <div style="width:95%;margin:auto">   
                            <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">Naši činnost podpořil</p>
                            <div class="text-center">
                                <img src="public/img/jihomoravsky_kraj.png" class="podporili_img" width="80%" alt="" class="rounded" style="margin-bottom:1rem">
                            </div>
                        </div> 
                    </div>
                    <div class="blurBackground schuzka_rozlozeni" >
                        <div style="width:95%;margin:auto">
                            <p style="font-family: skautbold;font-style: bold;text-align: center;" class="schuzky_header">Kalendář akcí</p>
                            <div>
                                <iframe src="https://calendar.google.com/calendar/embed?height=400&wkst=2&bgcolor=%23ffffff&ctz=Europe%2FPrague&src=OW5qdXI4YjYzbGUwNmt0dXBtZ2ZrcXJwOW9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&src=Y3MuY3plY2gjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&color=%23EF6C00&color=%230B8043&showNav=1&showTitle=0&showCalendars=0&showTabs=1&showPrint=0&showTz=0&showDate=1&mode=AGENDA" style="border-width:0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
<script>
window.onload = function() {
  var imgs = document.querySelectorAll('div.content_rozlozeni img');
  [].forEach.call(imgs, function(element, index, array) {
    element.classList.add("img-fluid");
  });
  $(".img-fluid").each(function(){
    var width = $(this).parent().width();
    var img_width = $(this).width();
    var rel_width = img_width / width;
    $(this).css("width",rel_width*100+"%");
  });
}

function passwordResetModalClicked(){
    document.getElementById("resetPasswordInput").value = "<?php echo getUserEmail()?>";
}
</script>
</html>