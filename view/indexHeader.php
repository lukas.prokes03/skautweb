<div class="header">
        <div class="row">
            <div class="col-md-8 mainLogo">
                <a href="../<?php if(isAdmin()){echo "admin/adminIndex";}?>">
                    <img src="../public/img/SKAUT_logo.png" class="logo_header" alt="logo">
                    <div class="logoText">
                        <h1 class="logo_text_h1 mb-0">Středisko Devíti křížů</h1>
                        <h2 class="logo_text_h1">Domašov</h2>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="menu">
        <nav>
            <div class="container-fluid" >
                <ul class="nav nav-justified" >
                    <div class="mainmenu-collapse collapsed">
                        <?php 
                            if (isAdmin()){
                                echo '
                                    <li class="nav-item" >
                                        <a class="nav-link" aria-current="page" href="../admin/adminIndex">Aktuality</a>
                                    </li>
                                ';
                            }else{ 
                                echo '
                                    <li class="nav-item" >
                                        <a class="nav-link" aria-current="page" href="/">Aktuality</a>
                                    </li>
                                ';
                            } 
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-toggle="dropdown"
                                data-hover="dropdown"aria-expanded="false">O nás</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/naseStredisko">Naše středisko a oddíly</a></li>
                                <li><a class="dropdown-item" href="https://www.skaut.cz/skauting" target="_blank" >Co je skauting</a></li>
                                <li><a class="dropdown-item" href="/naseKlubovny">Naše Klubovny</a></li>
                                <li><a class="dropdown-item" href="/loukaPodDomasovem">Skautská louka pod Domašovem</a></li>
                                <li><a class="dropdown-item" href="/taboristeJamolice">Tábořiště u Jamolic</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-toggle="dropdown"
                                data-hover="dropdown"
                                aria-expanded="false">Družiny</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/roveri">Roveři</a></li>
                                <li><a class="dropdown-item" href="/skauti">Skauti</a></li>
                                <li><a class="dropdown-item" href="/skautky">Skautky</a></li>
                                <li><a class="dropdown-item" href="/vlcata">Vlčata</a></li>
                                <li><a class="dropdown-item" href="/svetlusky">Světlušky</a></li>
                                <li><a class="dropdown-item" href="/benjaminci">Benjamínci</a></li>
                            </ul>
                        </li>

                        <?php 
                            if (isAdmin()){
                                echo '
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle"
                                            href="#"
                                            id="navbarDropdown"
                                            role="button"
                                            data-toggle="dropdown"
                                            data-hover="dropdown"
                                            aria-expanded="false">Galerie</a> 
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="../admin/uploadImagesForm">Nahrávání do galerie</a></li>
                                            <li><a class="dropdown-item" href="../admin/adminGalerie">Galerie</a></li>
                                        </ul>
                                    </li>
                                ';
                            }else{ 
                                echo '
                                    <li class="nav-item">
                                        <a class="nav-link" aria-current="page" href="/Galerie">Galerie</a>
                                    </li>
                                ';
                            } 
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="/keStazeni">Ke stažení</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="/kontakt">Kontakt</a>
                        </li>         
                    </div>
                    <button class="btn d-sm-inline d-md-none mainmenu-collapser menuButton" ><i class="fas fa-bars menuIcon"></i></button>
                </ul>  
            </div>
        </nav>
    </div>
</div>

<script>
    $(".mainmenu-collapser").click(function(){
        if(!$(".mainmenu-collapse").hasClass("collapsed")){
            $(".mainmenu-collapse").addClass("collapsed");
            return;
        }
        if($(".mainmenu-collapse").hasClass("collapsed")){
            $(".mainmenu-collapse").removeClass("collapsed");
            return;
        }
    });
</script>