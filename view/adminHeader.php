<div class="header">
    <div class="row">
        <div class="col-md-8 mainLogo">
            <a href="../admin/adminIndex">
                <img src="../public/img/SKAUT_logo.png" class="logo_header" alt="logo">
                <div class="logoText">
                    <h1 class="logo_text_h1 mb-0">Středisko Devíti křížů</h1>
                    <h2 class="logo_text_h1">Domašovv</h2>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <?php 
                if (isLogged()){
                    echo "
                    <div style='text-align:right' class='userInfo'>
                        <p class='user_text_header' style='margin-bottom:0rem'>Přihlášen jako ".getFullName()."</p>";
                    if (isAdmin()){
                        echo "<p class='user_text_header mb-0' ><a href='../client/register.php'>Vytvořit nového uživatele</a></p> ";
                    }
                    echo "
                        <p class='user_text_header' style='margin-bottom:0.2rem'><a href='../client/logout.php'>Odhlásit se</a></p>
                    </div>";
                }else{
                    echo "
                    <div style='text-align:right'>
                        <p class='user_text_header'><a href='../client/adminLogin.php'>Přihlásit se</a></p>
                    </div>";
                }
            ?>
        </div>
    </div>
    <div class="menu">
        <nav>
            <div class="container-fluid" >
                <ul class="nav  nav-justified">
                    <div class="mainmenu-collapse collapsed">
                        <li class="nav-item" >
                            <a class="nav-link" aria-current="page" href="../admin/adminIndex">Aktuality</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-toggle="dropdown"
                                aria-expanded="false">O nás</a>  
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../admin/adminNaseStredisko">Naše středisko a oddíly</a></li>
                                <li><a class="dropdown-item" href="https://www.skaut.cz/skauting" target="_blank" >Co je skauting</a></li>
                                <li><a class="dropdown-item" href="../admin/adminNaseKlubovny">Naše Klubovny</a></li>
                                <li><a class="dropdown-item" href="../admin/adminLoukaPodDomasovem">Skautská louka pod Domašovem</a></li>
                                <li><a class="dropdown-item" href="../admin/adminTaboristeJamolice">Tábořiště u Jamolic</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-toggle="dropdown"
                                data-hover="dropdown"
                                aria-expanded="false">Družiny</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../admin/adminRoveri">Roveři</a></li>
                                <li><a class="dropdown-item" href="../admin/adminSkauti">Skauti</a></li>
                                <li><a class="dropdown-item" href="../admin/adminSkautky">Skautky</a></li>
                                <li><a class="dropdown-item" href="../admin/adminVlcata">Vlčata</a></li>
                                <li><a class="dropdown-item" href="../admin/adminSvetlusky">Světlušky</a></li>
                                <li><a class="dropdown-item" href="../admin/adminBenjaminci">Benjamínci</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-toggle="dropdown"
                                data-hover="dropdown"
                                aria-expanded="false">Galerie</a> 
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../admin/uploadImagesForm">Nahrávání do galerie</a></li>
                                <li><a class="dropdown-item" href="../admin/adminGalerie">Galerie</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="../admin/adminKeStazeni">Ke stažení</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="../admin/adminKontakt">Kontakt</a>
                        </li>
                    </div>     
                    <button  class="btn  d-sm-inline d-md-none mainmenu-collapser menuButton" ><i class="fas fa-bars menuIcon"></i></button>  
                </ul>   
            </div>
        </nav>
    </div>
</div>
<script>
    $(".mainmenu-collapser").click(function(){
        if(!$(".mainmenu-collapse").hasClass("collapsed")){
            $(".mainmenu-collapse").addClass("collapsed");
            return;
        }
        if($(".mainmenu-collapse").hasClass("collapsed")){
            $(".mainmenu-collapse").removeClass("collapsed");
            return;
        }
    });
</script>