<?php 
include ("../imports/client.php");
adminKick();
include("../admin/config.php");
$con = new mysqli($host, $user, $psw, $db);

// Vytvoření alba a následné nahrání obrázků do něj 
if(isset($_POST['submitAlbum'])){
    $rok = $_POST['rok'];
    $album = rtrim($_POST['album']);
    $thumbnail = $_FILES["thumbnailAlbum"];

    if(is_dir("../gallery_images/".$rok."/".$album)){
        if(is_dir("../gallery_images/".$rok."/".$album."/thumbnail") == false){
            mkdir("../gallery_images/".$rok."/".$album."/thumbnail");
        }
        uploadThumbnail(FALSE,TRUE,$thumbnail,TRUE);
    }else{
        mkdir("../gallery_images/".$rok."/".$album);
        mkdir("../gallery_images/".$rok."/".$album."/thumbnail");
        uploadThumbnail(FALSE,TRUE,$thumbnail,FALSE);
    }
}

// Vytvoření roku a alba a následné nahrání obrázků do něj 
if(isset($_POST['submitRokAlbum'])){
    $rok = rtrim($_POST['rok']);
    $album = rtrim($_POST['album']);
    $files = $_FILES['files'];

    if(isset($_FILES["thumbnailAlbum"]) && isset($_FILES["thumbnailRok"])){
        $thumbnailAlbum = $_FILES["thumbnailAlbum"];
        $thumbnailRok= $_FILES["thumbnailRok"];
    }

    if(isset($thumbnailAlbum) && isset($thumbnailRok)){
        //Vytvoření složky rok
        if(is_dir("../gallery_images/".$rok)){
            uploadThumbnail(TRUE,FALSE,$thumbnailRok,TRUE);
        }else{
            mkdir("../gallery_images/".$rok);
            uploadThumbnail(TRUE,FALSE,$thumbnailRok,FALSE);
        }

        //Vytvoření složky alba a thumbnailu
        if(is_dir("../gallery_images/".$rok."/".$album)){
            if(is_dir("../gallery_images/".$rok."/".$album."/thumbnail") == FALSE){
                mkdir("../gallery_images/".$rok."/".$album."/thumbnail");
            }
            uploadThumbnail(FALSE,TRUE,$thumbnailAlbum,TRUE);
        }else{
            mkdir("../gallery_images/".$rok."/".$album);
            mkdir("../gallery_images/".$rok."/".$album."/thumbnail");
            uploadThumbnail(FALSE,TRUE,$thumbnailAlbum,FALSE);
        }
    }

    for ($i = 0; $i < count($files["name"]); $i++){
        $file = array("name" => $files["name"][$i], "size" => $files["size"][$i],"tmp_name" => $files["tmp_name"][$i]);
        uploadImage($rok,$album,$file);
    }
}

function correctImageOrientation($filename,$orientation) {
    if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
        case 3:
            $deg = 180;
            break;
        case 6:
            $deg = 270;
            break;
        case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
        $img = imagerotate($img, $deg, 0);        
        }
        imagejpeg($img, $filename, 95);
    }  
}

function load_image($filename, $type) {
    if( $type == IMAGETYPE_JPEG ) {
        $image = imagecreatefromjpeg($filename);
    }
    elseif( $type == IMAGETYPE_PNG ) {
        $image = imagecreatefrompng($filename);
    }
    elseif( $type == IMAGETYPE_GIF ) {
        $image = imagecreatefromgif($filename);
    }
    return $image;
}

function resize_image($new_width, $new_height, $image, $width, $height) {
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    return $new_image;
}

function resize_image_to_width($new_width, $image, $width, $height) {
    $resize_ratio = $new_width / $width;
    $new_height = $height * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function resize_image_to_height($new_height, $image, $width, $height) {
    $resize_ratio = $new_height / $height;
    $new_width = $width * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function generateRandomName(){
    $length = 8;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $real_string_length = strlen($characters) ;     
    $string="";

    for ($p = 0; $p < $length; $p++){
        $string .= $characters[mt_rand(0, $real_string_length-1)];
    }
    return $string;
}

function uploadThumbnail($isYear,$isAlbum,$file,$exists){
    global $rok, $album, $con;
    if ($isYear){
        $target_dir = "../gallery_thumbnails/years/";
    }elseif ($isAlbum) {
        $target_dir = "../gallery_thumbnails/albums/";
    }

    $thumbnailOk = 1;
    $imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));
    $imageName = generateRandomName();
    $target_file = $target_dir . $imageName .".". $imageFileType;
    
    //Kontrola existence $target_dir
    if (!file_exists($target_dir)) {
        $uploadOk = 0;
    }
    
    // Check if file already exists
    if (file_exists($target_file)) {
        $thumbnailOk = 0;
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $thumbnailOk = 0;
    }
    
    // Check if $thumbnailOk is set to 0 by an error
    if ($thumbnailOk == 0) {
        //echo "Sorry, your file was not uploaded.";
    } else {
        if($imageFileType == "jpg" || $imageFileType == "jpeg" ) {
            $orientation = exif_read_data($file["tmp_name"])["Orientation"];
        }else{
            $orientation = 1;
        }

        if ($isYear){
            if($exists){
                //Smazání souboru thumbnailu roku
                $stmt = $con->prepare("SELECT thumb.id, thumb.path FROM galleryThumbnails thumb WHERE thumb.rok = ? AND thumb.album =''");
                $stmt->bind_param('s', $rok);
                $stmt->execute();
                $result = $stmt->get_result();
                $oldThumbnailPath = $result->fetch_array()["path"];

                unlink($oldThumbnailPath);

                $stmt = $con->prepare("UPDATE galleryThumbnails SET path=? WHERE rok=? AND album='' ");
                $stmt->bind_param('ss', $target_file, $rok);
                $stmt->execute();
            }else{
                $stmt = $con->prepare("INSERT INTO galleryThumbnails (rok, path) VALUES (?, ?)");
                $stmt->bind_param('ss', $rok, $target_file);
                $stmt->execute();
            }    
        }elseif ($isAlbum) {
            if($exists){
                //Smazání souboru thumbnailu alba
                $stmt = $con->prepare("SELECT thumb.id, thumb.path FROM galleryThumbnails thumb WHERE thumb.rok = ? AND thumb.album = ?");
                $stmt->bind_param('ss', $rok, $album);
                $stmt->execute();
                $result = $stmt->get_result();
                $oldThumbnailPath = $result->fetch_array()["path"];

                unlink($oldThumbnailPath);

                $stmt = $con->prepare("UPDATE galleryThumbnails SET path=? WHERE rok=? AND album=? ");
                $stmt->bind_param('sss', $target_file, $rok, $album);
                $stmt->execute();
            }else{
                $stmt = $con->prepare("INSERT INTO galleryThumbnails (rok, album, path) VALUES (?, ?, ?)");
                $stmt->bind_param('sss', $rok, $album , $target_file);
                $stmt->execute();
            }
        }
        list($width, $height, $type) = getimagesize($file["tmp_name"]);
        $old_image = load_image($file["tmp_name"], $type);

        if ($width>$height){
            $image_fixed_thumbnail = resize_image_to_width(200, $old_image, $width, $height);
        }else{
            $image_fixed_thumbnail = resize_image_to_height(200, $old_image, $width, $height);
        }

        imagejpeg($image_fixed_thumbnail, $target_file , 100);
        correctImageOrientation($target_file,$orientation);
        imagedestroy($image_fixed_thumbnail);
    }
    echo"{ok:".$thumbnailOk.",msg:'thumbnail msg'}";
}

function uploadImage($rok,$album,$file){
    global $rok, $album, $con;
    $target_dir = "../gallery_images/".$rok."/".$album."/";
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));
    $imageName = generateRandomName();
    $target_file = $target_dir . $imageName .".". $imageFileType;
    $target_file_thumbnail = $target_dir ."thumbnail/". $imageName .".". $imageFileType;
    
    //Kontrola existence $target_dir
    if (!file_exists($target_dir)) {
        $uploadOk = 0;
        echo "{ok:false,msg:'Složka do které se snažíte uložit obrázek neexistuje'}";
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = 0;
        echo "{ok:false,msg:'Sorry, file already exists'}";
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $uploadOk = 0;
        echo '{"ok":false,"msg":"only JPG, JPEG, PNG & GIF files are allowed"}';
    }
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "Sorry, your file was not uploaded.";
    } else {
        if($imageFileType == "jpg" || $imageFileType == "jpeg" ) {
            $orientation = exif_read_data($file["tmp_name"])["Orientation"];
        }else{
            $orientation = 1;
        }

        $stmt = $con->prepare("INSERT INTO images (rok, album, name, filetype) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('ssss', $rok, $album , $imageName, $imageFileType);
        $stmt->execute();

        list($width, $height, $type) = getimagesize($file["tmp_name"]);
        $old_image = load_image($file["tmp_name"], $type);
        
        if ($width>$height){
            $image_fixed_thumbnail = resize_image_to_width(150, $old_image, $width, $height);
            $image_fixed = resize_image_to_width(1280, $old_image, $width, $height);
        }else{
            $image_fixed_thumbnail = resize_image_to_height(150, $old_image, $width, $height);
            $image_fixed = resize_image_to_height(1280, $old_image, $width, $height);
        }
        imagejpeg($image_fixed, $target_file, 100);
        correctImageOrientation($target_file,$orientation);
        imagedestroy($image_fixed);
        imagejpeg($image_fixed_thumbnail, $target_file_thumbnail , 100);
        correctImageOrientation($target_file_thumbnail,$orientation);
        imagedestroy($image_fixed_thumbnail);

        echo '{"ok":true,"msg":"ok"}';
    }
}
?>