<?php 
include("../admin/config.php");

if(isset($_POST['year']) && isset($_POST['album'])){
    $page = (int)$_POST["page"]*20;

    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("SELECT * FROM `images` WHERE rok = ? AND album = ? LIMIT 20 OFFSET ?");
    $stmt->bind_param("ssi",$_POST["year"], $_POST["album"],$page);
    $stmt->execute();
    $result = $stmt->get_result();
    $images_arr = array();
    while ($row = $result->fetch_array()) {
        $images_arr[] = $row;
    }

    $stmt = $con->prepare("SELECT COUNT(id) FROM `images` WHERE rok = ? AND album = ?");
    $stmt->bind_param("ss",$_POST["year"], $_POST["album"]);
    $stmt->execute();
    $result = $stmt->get_result();
    $count=$result->fetch_array()[0];

    $object = (object) ['count' => $count,'images'=>$images_arr];
    echo(json_encode($object));
    http_response_code(200);
}
?>