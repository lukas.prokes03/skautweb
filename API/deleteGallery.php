<?php 
include ("../imports/client.php");
adminKick();
include("../admin/config.php");

$con = new mysqli($host, $user, $psw, $db);
function deleteDir($dirPath) {
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

if(isset($_POST["deleteRok"])){
    //Odstraní rok a všechny jeho podalba
    $rok = $_POST["rok"];

    $stmt = $con->prepare("SELECT path FROM galleryThumbnails WHERE rok = ?");
    $stmt->bind_param("s",$rok);
    $stmt->execute();
    $result = $stmt->get_result();
    $thumbnailPathArray = array();

    while ($row = $result->fetch_array()) {
        $thumbnailPathArray[] = $row;
    }

    for ($i = 0; $i < count($thumbnailPathArray); $i++){
        unlink($thumbnailPathArray[$i]["path"]);
    }

    $stmt = $con->prepare("DELETE FROM galleryThumbnails WHERE rok = ? ");
    $stmt->bind_param("s",$rok);
    $stmt->execute();
    $stmt = $con->prepare("DELETE FROM images WHERE rok = ?");
    $stmt->bind_param("s",$rok);
    $stmt->execute();

    deleteDir("../gallery_images/".$rok."/");

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
}


if(isset($_POST["deleteAlbum"])){
    //Odstraní album 
    $albumId = (int)$_POST["albumId"];
    $album = $_POST["album"];
    $path = $_POST["path"];

    $stmt = $con->prepare("SELECT rok FROM galleryThumbnails WHERE id = ?");
    $stmt->bind_param("i",$albumId);
    $stmt->execute();
    $result = $stmt->get_result();
    $rok = $result->fetch_array()[0];

    $stmt = $con->prepare("DELETE FROM galleryThumbnails WHERE album = ? ");
    $stmt->bind_param("s",$album);
    $stmt->execute();

    $stmt = $con->prepare("DELETE FROM images WHERE rok = ? AND album = ?");
    $stmt->bind_param("ss",$rok,$album);
    $stmt->execute();

    deleteDir("../gallery_images/".$rok."/".$album."/");
    unlink($path);
    
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
}
?>