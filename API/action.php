<?php 
include ("../imports/client.php");
adminKick();

if(isset($_POST["action"])){
    //Returns whole select element of every year and empty album select
    if($_POST["action"] == "fetch"){
        $folder = array_filter(glob('../gallery_images/*'), 'is_dir');
        $folder = array_reverse($folder);
        $output = '
        <select name="rok" id="album_year" class="form-select" required>
        <option hidden value="" >*Vyberte Rok</option>
        ';
        if(count($folder) > 0 ){
            foreach($folder as $name){
                $year_name = substr($name,-4);
                $output .='
                <option value="'.$year_name.'">'.$year_name.'</option>
                ';
            }
        }else{
            $output .= '
            <li><a class="dropdown-item" href="#">Nebyla nalezena žádná složka</a></li>
            ';
        }
        $output .= '</select>';

        //Empty album select
        $output .= '
        <select name="album" id="album_album" class="form-select" required>
        <option hidden value="" >*Vyberte Album</option>
        </select>
        ';
        echo $output;
    }

    //Returns whole select element of every year
    if($_POST["action"] == "fetchRok"){
        $folder = array_filter(glob('../gallery_images/*'), 'is_dir');
        $folder = array_reverse($folder);
        $output = '
        <select name="rok" id="album_year" class="form-select" required>
        <option hidden value="" >*Vyberte Rok</option>
        ';
        if(count($folder) > 0 ){
            foreach($folder as $name){
                $year_name = substr($name,-4);
                $output .='
                <option value="'.$year_name.'">'.$year_name.'</option>
                ';
            }
        }else{
            $output .= '
            <li><a class="dropdown-item" href="#">Nebyla nalezena žádná složka</a></li>
            ';
        }
        $output .= '</select>';
        echo $output;
    }
}


//Returns array of albums in specific year
if(isset($_POST["rok"])){
    $folder = array_filter(glob('../gallery_images/'.$_POST["rok"].'/*'), 'is_dir');
    $arr_names = array();
    if(count($folder) > 0 ){
        foreach($folder as $name){
            $year_name = substr($name,23);
            $arr_names[] = $year_name;
        }
    }
    echo(json_encode($arr_names));
}
?>