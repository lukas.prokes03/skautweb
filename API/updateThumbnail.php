<?php 
include ("../imports/client.php");
adminKick();
include("../admin/config.php");

$con = new mysqli($host, $user, $psw, $db);

//Update thumbnailu Alba
if(isset($_POST["submitAlbum"])){
    $path = $_POST['path'];
    $album = rtrim($_POST['album']);
    $thumbnail = $_FILES["thumbnailAlbum"];
    uploadThumbnail(FALSE,TRUE,$thumbnail,TRUE);
}

//Update název Alba
if(isset($_POST["submitAlbumName"])){
    $path = $_POST['path'];
    $album = rtrim($_POST['album']);
    $new_name = $_POST["albumName"];
    UpdateName(FALSE,TRUE,$new_name);
}

//Update thumbnailu roku
if(isset($_POST["submitRok"])){
    $rok = $_POST['rok'];
    $thumbnail = $_FILES["thumbnailRok"];
    uploadThumbnail(TRUE,FALSE,$thumbnail,TRUE);
}

//Update název roku
if(isset($_POST["submitRokName"])){
    $rok = $_POST['rok'];
    $new_name = $_POST["rokName"];
    updateName(TRUE,FALSE,$new_name);
}

function updateName($isYear,$isAlbum,$new_name){
    global $path, $con, $album, $rok;

    if ($isYear){
        $yearPaths = array_filter(glob('../gallery_images/*'), 'is_dir');
        $yearNames = array();
        foreach($yearPaths as $name){
            array_push($yearNames,substr($name,-4));
        }
        if(in_array($new_name,$yearNames)){
            echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
        }else{
            //databaze thumbnails
            $stmt = $con->prepare("UPDATE galleryThumbnails SET rok=? WHERE rok=? ");
            $stmt->bind_param('ss', $new_name, $rok);
            $stmt->execute();    
            //databáze images
            $stmt = $con->prepare("UPDATE images SET rok=? WHERE rok=? ");
            $stmt->bind_param('ss', $new_name, $rok);
            $stmt->execute();
            //soubory
            $old_year = "../gallery_images/".$rok;
            $new_year = "../gallery_images/".$new_name;
            rename($old_year, $new_year);
        }
    }elseif ($isAlbum) {
        $stmt = $con->prepare("SELECT thumb.id, thumb.rok FROM galleryThumbnails thumb WHERE thumb.path = ? AND thumb.album = ?");
        $stmt->bind_param('ss', $path, $album);
        $stmt->execute();
        $result = $stmt->get_result();
        $rok = $result->fetch_array()["rok"];

        $albumPaths = array_filter(glob('../gallery_images/'.$rok.'/*'), 'is_dir');
        $albumNames = array();
        foreach($albumPaths as $name){
        array_push($albumNames,substr($name,23));
        }
        if(in_array($new_name,$albumNames)){
            echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
        }else{
            //databaze thumbnails
            $stmt = $con->prepare("UPDATE galleryThumbnails SET album=? WHERE rok=? AND album = ?");
            $stmt->bind_param('sss', $new_name, $rok, $album);
            $stmt->execute();  
            //databáze images
            $stmt = $con->prepare("UPDATE images SET album=? WHERE rok=? AND album=? ");
            $stmt->bind_param('sss', $new_name, $rok, $album);
            $stmt->execute();
            //soubory
            $old_album = "../gallery_images/".$rok."/".$album;
            $new_album = "../gallery_images/".$rok."/".$new_name;
            rename($old_album, $new_album);
        }
    }
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
}


function uploadThumbnail($isYear,$isAlbum,$file,$exists){
    global $rok, $album, $con, $path;
    if ($isYear){
        $target_dir = "../gallery_thumbnails/years/";
    }elseif ($isAlbum) {
        $target_dir = "../gallery_thumbnails/albums/";
    }

    
    $thumbnailOk = 1;
    $imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));
    $imageName = generateRandomName();
    $target_file = $target_dir . $imageName .".". $imageFileType;
    
    //Check if $target_dir exists
    if (!file_exists($target_dir)) {
        $uploadOk = 0;
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        $thumbnailOk = 0;
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $thumbnailOk = 0;
    }
    
    // Check if $thumbnailOk is set to 0 by an error
    if ($thumbnailOk == 0) {
        //echo "Sorry, your file was not uploaded.";
    } else {
        if($imageFileType == "jpg" || $imageFileType == "jpeg" ) {
            $orientation = exif_read_data($file["tmp_name"])["Orientation"];
        }else{
            $orientation = 1;
        }

        if ($isYear){
            $stmt = $con->prepare("SELECT thumb.id, thumb.path FROM galleryThumbnails thumb WHERE thumb.rok = ? AND thumb.album =''");
            $stmt->bind_param('s', $rok);
            $stmt->execute();
            $result = $stmt->get_result();
            $oldThumbnailPath = $result->fetch_array()["path"];

            unlink($oldThumbnailPath);

            $stmt = $con->prepare("UPDATE galleryThumbnails SET path=? WHERE rok=? AND album='' ");
            $stmt->bind_param('ss', $target_file, $rok);
            $stmt->execute();    
        }elseif ($isAlbum) {
            //Smazání souboru thumbnailu
            $stmt = $con->prepare("SELECT thumb.id, thumb.rok FROM galleryThumbnails thumb WHERE thumb.path = ? AND thumb.album = ?");
            $stmt->bind_param('ss', $path, $album);
            $stmt->execute();
            $result = $stmt->get_result();
            $rok = $result->fetch_array()["rok"];

            $oldThumbnailPath = $path;

            unlink($oldThumbnailPath);

            $stmt = $con->prepare("UPDATE galleryThumbnails SET path=? WHERE rok=? AND album=? ");
            $stmt->bind_param('sss', $target_file, $rok, $album);
            $stmt->execute();
        }
        list($width, $height, $type) = getimagesize($file["tmp_name"]);
        $old_image = load_image($file["tmp_name"], $type);

        if ($width>$height){
            $image_fixed_thumbnail = resize_image_to_width(200, $old_image, $width, $height);
        }else{
            $image_fixed_thumbnail = resize_image_to_height(200, $old_image, $width, $height);
        }

        imagejpeg($image_fixed_thumbnail, $target_file , 100);
        correctImageOrientation($target_file,$orientation);
        imagedestroy($image_fixed_thumbnail);

    }
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminGalerie\");</script>";
}

function correctImageOrientation($filename,$orientation) {
    if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
        case 3:
            $deg = 180;
            break;
        case 6:
            $deg = 270;
            break;
        case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
        $img = imagerotate($img, $deg, 0);        
        }
        imagejpeg($img, $filename, 95);
    }
}

function load_image($filename, $type) {
    if( $type == IMAGETYPE_JPEG ) {
        $image = imagecreatefromjpeg($filename);
    }
    elseif( $type == IMAGETYPE_PNG ) {
        $image = imagecreatefrompng($filename);
    }
    elseif( $type == IMAGETYPE_GIF ) {
        $image = imagecreatefromgif($filename);
    }
    return $image;
}

function resize_image($new_width, $new_height, $image, $width, $height) {
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    return $new_image;
}

function resize_image_to_width($new_width, $image, $width, $height) {
    $resize_ratio = $new_width / $width;
    $new_height = $height * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function resize_image_to_height($new_height, $image, $width, $height) {
    $resize_ratio = $new_height / $height;
    $new_width = $width * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function generateRandomName(){
    $length = 8;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $real_string_length = strlen($characters) ;     
    $string="";

    for ($p = 0; $p < $length; $p++){
        $string .= $characters[mt_rand(0, $real_string_length-1)];
    }
    return $string;
}
?>