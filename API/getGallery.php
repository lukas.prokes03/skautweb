<?php 
include("../admin/config.php");

$con = new mysqli($host, $user, $psw, $db);
$stmt = $con->prepare("SELECT img.rok, thumb.path FROM images img, galleryThumbnails thumb WHERE img.rok = thumb.rok AND thumb.album = '' GROUP BY rok ORDER BY img.rok desc");
$stmt->execute();
$result = $stmt->get_result();
$years_arr = array();
while ($row = $result->fetch_array()) {
    $years_arr[] = $row;
}
echo(json_encode($years_arr));
http_response_code(200);
?>