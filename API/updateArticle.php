<?php
include ("../imports/client.php");
adminKick();
include("../admin/config.php");
include("../imports/phpMailer.php");


$con = new mysqli($host, $user, $psw, $db);
if(isset($_POST['delete'])){
    $aid = $_POST['aid'];
    $poradi = $_POST['poradi'];
    if($poradi != 0){
        $stmt = $con->prepare("SELECT SUM(CASE WHEN poradi>0 THEN 1 ELSE 0 END) as total FROM articles");
        $stmt->execute();
        $result = $stmt->get_result();
        $pocet = $result->fetch_array()[0];

        $clanky_pro_precislovani = $pocet - $poradi;
        $aktualni_poradi = $poradi;
        for ($i=0; $i<= $clanky_pro_precislovani; $i++){
            $aktualni_poradi = $aktualni_poradi + 1 ;
            $stmt = $con->prepare("UPDATE articles SET poradi=poradi-1 WHERE poradi= ? ");
            $stmt->bind_param('i', $aktualni_poradi);
            $stmt->execute();
        }  
    }
    $stmt = $con->prepare("DELETE FROM articles WHERE id= ? ");
    $stmt->bind_param('i', $aid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
}elseif (isset($_POST['save'])) {
    $lastChange = date('Y-m-d H:i:s');
    $editorContent = $_POST['editor1'];
    $title = $_POST['title'];
    $aid = $_POST['aid'];

    $stmt = $con->prepare("UPDATE `articles` SET `title`=?,`description`=?,`lastChange`=? WHERE id=?;");
    $stmt->bind_param('sssi', $title,$editorContent,$lastChange,$aid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
}elseif (isset($_POST['archivovat'])){
    $lastChange = date('Y-m-d H:i:s');
    $aid = $_POST['aid'];
    $poradi = $_POST['poradi'];
    if($poradi != 0){
        $stmt = $con->prepare("SELECT SUM(CASE WHEN poradi>0 THEN 1 ELSE 0 END) as total FROM articles");
        $stmt->execute();
        $result = $stmt->get_result();
        $pocet = $result->fetch_array()[0];

        $clanky_pro_precislovani = $pocet - $poradi;
        $aktualni_poradi = $poradi;
        for ($i=0; $i<= $clanky_pro_precislovani; $i++){
            $aktualni_poradi = $aktualni_poradi + 1 ;
            $stmt = $con->prepare("UPDATE articles SET poradi=poradi-1 , `lastChange`=? WHERE poradi=?");
            $stmt->bind_param('si', $lastChange,$aktualni_poradi);
            $stmt->execute();
        }  
        $stmt = $con->prepare("UPDATE articles SET poradi=0 WHERE id=?");
        $stmt->bind_param('i', $aid);
        $stmt->execute();
    }
    $stmt = $con->prepare("UPDATE `articles` SET `enable`='0' WHERE id=?");
    $stmt->bind_param('i', $aid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
}elseif (isset($_POST['odArchivovat'])){
    $lastChange = date('Y-m-d H:i:s');
    $aid = $_POST['aid'];
    $poradi = $_POST['poradi'];
    
    $stmt = $con->prepare("SELECT SUM(CASE WHEN poradi>0 THEN 1 ELSE 0 END) as total FROM articles");
    $stmt->execute();
    $result = $stmt->get_result();
    $pocet = $result->fetch_array()[0];

    $aktualni_poradi = $pocet+1;   
    $stmt = $con->prepare("UPDATE articles SET `poradi`=? , `lastChange`=?, `enable`='1' WHERE id=?");
    $stmt->bind_param('isi', $aktualni_poradi, $lastChange,$aid);
    $stmt->execute();

    $stmt = $con->prepare("SELECT title, description FROM articles WHERE id = ?");
    $stmt->bind_param('i', $aid);
    $stmt->execute();
    $result = $stmt->get_result();
    $aktualita = $result->fetch_array();

    $title =$aktualita["title"];
    $description =$aktualita["description"];

    $subject = "Novinka na webu www.skaut-domasov.cz - ".$title;

    $stmt = $con->prepare("SELECT email FROM subscribers");
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_array()) {
        $email = $row["email"];
        $body = "
            <p style='text-align: center;font-size: 1.3rem;margin-bottom: 1.5rem;margin-top:0;'>".$title."</p>
            ".$description."<br>";
        $footer = ", <a href='".$webDomainName."/API/unsubscribe?email=".$email."'>Odhlásit se z odběru novinek</a>";
        sendEmail($email,$subject,$body,$footer);
    }
    
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
}elseif (isset($_POST['up'])){
    $aid = $_POST['aid'];
    $poradi = $_POST['poradi'];

    $stmt = $con->prepare("SELECT SUM(CASE WHEN poradi>0 THEN 1 ELSE 0 END) as total FROM articles");
    $stmt->execute();
    $result = $stmt->get_result();
    $pocet = $result->fetch_array()[0];

    if($poradi==$pocet){
        $poradi_nasledujici = $poradi;
    }else{
        $poradi_nasledujici = $poradi+1;
    }
    $stmt = $con->prepare("UPDATE `articles` SET poradi=? WHERE poradi=?");
    $stmt->bind_param('ii', $poradi, $poradi_nasledujici);
    $stmt->execute();

    $stmt = $con->prepare("UPDATE `articles` SET poradi=? WHERE id=?");
    $stmt->bind_param('ii', $poradi_nasledujici, $aid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>"; 
}elseif (isset($_POST['down'])){
    $aid = $_POST['aid'];
    $poradi = $_POST['poradi'];

    if($poradi==1){
        $poradi_predchozi = $poradi;
    }else{
        $poradi_predchozi = $poradi-1;
    }
    
    $stmt = $con->prepare("UPDATE `articles` SET poradi=? WHERE poradi=?");
    $stmt->bind_param('ii', $poradi, $poradi_predchozi);
    $stmt->execute();

    $stmt = $con->prepare("UPDATE `articles` SET poradi=? WHERE id=?");
    $stmt->bind_param('ii', $poradi_predchozi, $aid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>"; 
};

?>