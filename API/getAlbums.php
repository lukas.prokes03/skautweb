<?php 
include("../admin/config.php");

if(isset($_POST['year'])){
    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("SELECT img.album album, thumb.path path, thumb.id id FROM images img, galleryThumbnails thumb WHERE img.rok = ? AND img.rok = thumb.rok AND img.album = thumb.album GROUP BY album");
    $stmt->bind_param("s",$_POST["year"]);
    $stmt->execute();
    $result = $stmt->get_result();
    $albums_arr = array();
    while ($row = $result->fetch_array()) {
        $albums_arr[] = $row;
    }
    echo(json_encode($albums_arr));
    http_response_code(200);
}
?>