<?php
include ("../imports/client.php");
rootKick();
include("../admin/config.php");

$con = new mysqli($host, $user, $psw, $db);

$userId = $_POST['id'];

$stmt = $con->prepare("DELETE FROM users WHERE id= ? ");
$stmt->bind_param('i', $userId);
$stmt->execute();

echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";

?>