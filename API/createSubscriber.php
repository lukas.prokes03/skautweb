<?php 
include ("../imports/client.php");
include("../admin/config.php");
include("../imports/phpMailer.php");

if(isset($_POST['email'])){
    $email = $_POST['email'];
    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("INSERT INTO subscribers(email) SELECT * FROM (SELECT ?) AS tmp WHERE NOT EXISTS (SELECT * FROM subscribers WHERE email=?) LIMIT 1");
    $stmt->bind_param("ss",$email,$email);
    $stmt->execute();

    $footer = ", <a href='".$webDomainName."/API/unsubscribe?email=".$email."'>Odhlásit se z odběru novinek</a>";
    $body = "
            <p style='margin-top: 2rem !important;margin-bottom: 1rem !important;'>
            Tento e-mail byl úspěšně přihlášen k odběru novinek. 
            </p>
        ";
    sendEmail($email,"Přihlášení k odběru novinek",$body,$footer); 

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../\");</script>";
}else{
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../\");</script>";
}
?>