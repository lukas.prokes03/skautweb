<?php 
include ("../imports/client.php"); 
adminKick();
include("../admin/config.php"); 
include("../imports/phpMailer.php");

if ( ! empty( $_POST ) ) {
  if ( isset( $_POST['title'] ) && isset( $_POST['editorCreate'] )){
    $createdOn = date('Y-m-d H:i:s');
    $author = getUserId();

    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("SELECT SUM(CASE WHEN poradi>0 THEN 1 ELSE 0 END) as total FROM articles");
    $stmt->execute();
    $result = $stmt->get_result();
    $pocet = $result->fetch_array()[0];

    $poradi_clanku = $pocet + 1;
    $stmt = $con->prepare("INSERT INTO articles (title, description, createdOn, lastChange, author, enable, poradi) VALUES (?, ?, ?, ?, ?, 1, ?)");
    $stmt->bind_param('sssssi', $_POST['title'], $_POST['editorCreate'] , $createdOn, $createdOn, $author, $poradi_clanku);
    $stmt->execute();

    $subject = "Novinka na webu www.skaut-domasov.cz - ".$_POST['title'];

    $stmt = $con->prepare("SELECT email FROM subscribers");
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_array()) {
        $email = $row["email"];
        $body = "
            <p style='text-align: center;font-size: 1.3rem;margin-bottom: 1.5rem;margin-top:0;'>".$_POST['title']."</p>
            ".$_POST['editorCreate']."<br>";
        $footer = ", <a href='".$webDomainName."/API/unsubscribe?email=".$email."'>Odhlásit se z odběru novinek</a>";
        sendEmail($email,$subject,$body,$footer);
    }
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
  }else{
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
  }
}
?>