<?php 
include ("../imports/client.php");
adminKick();
include("../admin/config.php");

if(isset($_POST['oddil']) && isset($_POST['den']) && isset($_POST['zacatek']) && isset($_POST['konec'])){
    $sid = $_POST['sid'];
    $oddil = $_POST['oddil'];
    $den = $_POST['den'];

    if($den == "po" || $den == "PO"){
        $denCely = "pondělí";
    } elseif($den == "út" || $den == "ÚT"){
        $denCely = "úterý";
    } elseif($den == "st" || $den == "ST"){
        $denCely = "středa";
    } elseif($den == "čt" || $den == "ČT"){
        $denCely = "čtvrtek";
    } elseif($den == "pá" || $den == "PÁ"){
        $denCely = "pátek";
    } else{
        $denCely = $den;
    }
    $zacatek = $_POST['zacatek'];
    $konec = $_POST['konec'];

    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("UPDATE `schuzky` SET `oddil`=?,`den`=?,`denCely`=?,`zacatek`=?,`konec`=? WHERE id=?");
    $stmt->bind_param('sssssi', $oddil, $den, $denCely, $zacatek, $konec, $sid);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex.php\");</script>";      
}else{
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex.php\");</script>";
}
?>