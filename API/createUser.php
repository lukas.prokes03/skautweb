<?php 
include ("../imports/client.php");
adminKick();
include("../admin/config.php");

if ( ! empty( $_POST ) ) {
  if ( isset( $_POST['username'] ) && isset( $_POST['password'] ) && isset( $_POST['firstname'] ) && isset( $_POST['lastname'] ) && isset( $_POST["email"]) ){
    $role = 0;
    if(isRoot()){
        if (isset($_POST['role'])){
            $role = 1;
        }else{
            $role = 0;
        }
    }
    
    $cryptedpass = password_hash($_POST['password'], PASSWORD_BCRYPT);

    $con = new mysqli($host, $user, $psw, $db);
    $stmt = $con->prepare("INSERT INTO users (username, email, password, firstname, lastname, role) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->bind_param('sssssi', $_POST['username'], $_POST['email'] , $cryptedpass, $_POST['firstname'] , $_POST['lastname'], $role);
    $stmt->execute();

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex.php\");</script>"; 
  }else{
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../client/register.php\");</script>";
  }
}
?>