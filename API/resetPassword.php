<?php 
include("../admin/config.php"); 
include("../imports/phpMailer.php");
include("../imports/client.php");

function generateRandomString(){
    $length = 16;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $real_string_length = strlen($characters) ;     
    $string="";

    for ($p = 0; $p < $length; $p++){
        $string .= $characters[mt_rand(0, $real_string_length-1)];
    }
    return $string;
}

$con = new mysqli($host, $user, $psw, $db);
if(isset($_POST["odeslano"])){
    $email = $_POST['email'];

    $stmt = $con->prepare("SELECT email FROM users WHERE 1");
    $stmt->execute();
    $usersEmails = $stmt->get_result();
    $usersEmails_arr = array();
    while ($row = $usersEmails->fetch_array()) {
        $usersEmails_arr[] = $row["email"];
    }

    if(in_array($email,$usersEmails_arr)){
        $resetHash = generateRandomString();

        $stmt = $con->prepare("UPDATE users SET resetHash = ? WHERE email= ? ");
        $stmt->bind_param('ss', $resetHash,$email);
        $stmt->execute(); 

        $body = "
            <p style='margin-top: 2rem !important;margin-bottom: 1rem !important;'>
            Právě někdo požádal o změnu hesla k účtu, k němuž je přiřazený tento email. Pokud jste to byli vy, pokračujte kliknutím 
                <a href='".$webDomainName."/client/resetPassword?resetHash=".$resetHash."&name=reset&email=".$email."' style=>zde</a>. 
                Pokud jste to nebyli vy, můžete 
                <a href='".$webDomainName."/API/resetPassword?resetHash=".$resetHash."&cancel=cancel' style=>požadavek zrušit</a>.
            </p>
            <p>Tento odkaz lze využít pouze jednou</p>
        ";
        sendEmail($email,"Změna hesla",$body); 
    }else{
        $body = "
        <p style='margin-top: 2rem !important;margin-bottom: 1rem !important;'>
            Pokusili jste se změnit heslo na webu ...... , na kterém nejste zaregistrováni.
        </p>
        ";
        sendEmail($email,"Neregistrováný uživatel",$body);
    }
    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../client/adminLogin\");</script>";

}elseif(isset($_POST["zmena"])){
    $cryptedpass = password_hash($_POST['password'], PASSWORD_BCRYPT);

    $stmt = $con->prepare("SELECT CASE WHEN EXISTS (SELECT * FROM users WHERE resetHash = ?) THEN 'TRUE' ELSE 'FALSE' END");
    $stmt->bind_param('s',$_POST["resetHash"]);
    $stmt->execute();
    $result = $stmt->get_result();
    $exist = $result->fetch_array()[0];

    if(filter_var($exist,FILTER_VALIDATE_BOOLEAN)){
        $stmt = $con->prepare("UPDATE users SET password = ? WHERE resetHash= ? ");
        $stmt->bind_param('ss', $cryptedpass ,$_POST["resetHash"]);
        $stmt->execute();
        
        $stmt = $con->prepare("UPDATE users SET resetHash = NULL WHERE resetHash = ?");
        $stmt->bind_param('s',$_POST["resetHash"]);
        $stmt->execute();
    }else{
        $body = "
        <p style='margin-top: 2rem !important;margin-bottom: 1rem !important;'>
            Právě se někdo pokusil o změnu hesla k účtu, k němuž je přiřazený tento email, pomocí neplatného odkazu. Pokud jste to byli vy, požádejte o ověřovací email znovu.
            Pokud jste to vy nebyli, můžete tento e-mail ignorovat.
        </p>
        ";
        sendEmail($_POST["email"],"Neplatná změna hesla",$body);
    }

    echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../client/adminLogin\");</script>";
}elseif(isset($_GET["cancel"])){
    $resetHash = $_GET["resetHash"];

    $stmt = $con->prepare("UPDATE users SET resetHash = NULL WHERE resetHash = ?");
    $stmt->bind_param('s',$resetHash);
    $stmt->execute();
    echo "<script>window.close();</script>";
}
?>