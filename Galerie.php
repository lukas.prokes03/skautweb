<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
    <head>
        <title>Skaut Domašov | Galerie</title>
        <link href="lightGallery/css/lightgallery.css" rel="stylesheet"> 
        <?php include("imports/head.php") ?>
        <style>
            .lg-backdrop {
                background-color: rgba(255, 255, 255, 0.8);
            }
            .lg-icon{
                color: rgb(45, 45, 45) !important;
            }
            #lg-counter{
                color: rgb(45, 45, 45) !important;
            }
        </style>
    </head>
    <body>
        <?php include("imports/body.php") ?>
        <div class="page">
            <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
                <div class="page_container">
                    <p id="btnBack" style="text-align: left;margin-bottom:0">
                        <i onclick="back();" class="fas fa-arrow-left iconAdd" style="cursor:pointer"></i>
                        <span onclick="back();" style="cursor:pointer;font-size:1.4rem">Zpět<span>
                    </p>
                    <div class="row" id="gallery_albums_holder"></div>
                    <div class="row" id="lightgallery"></div>
                    <div class="row" id="nextPage" style="display:none">
                        <div onclick="nextPage()" id="nextPageButton" style="margin: auto;text-align: center;font-size: 1.6rem;cursor:pointer;width:10%;">
                            <i class="fas fa-angle-double-down" style="margin-bottom: 1.5rem"></i>
                        </div>
                    </div>
                </div>
            </div>  
            <?php include("view/indexFooter.php")?>
        </div>
    </body>
<script src="lightGallery/js/lightgallery.js"></script> 
<script src="lightGallery/js/lg-rotate.js"></script>    
<script src="lightGallery/js/lg-fullscreen.js"></script>
<script src="lightGallery/js/lg-zoom.js"></script>
<script src="lightGallery/js/lg-autoplay.js"></script>
<script>
    var currentPage = 0;
    var currentYear = null;
    var currentAlbum = null;

    function back(){
        if(currentYear != null && currentAlbum == null){
            //Back to onGallery
            onGallery();
            currentAlbum = null;
            currentYear = null;
        }

        if(currentYear != null && currentAlbum != null){
            //Back to albums
            onYearClicked(currentYear);
            document.getElementById("nextPage").style.display = "none";
            currentAlbum = null;
            currentPage = 0;
        }
    }
    function nextPage(){
        currentPage++;
        onAlbumClicked(currentAlbum,currentPage);
    }
    
    function onAlbumClicked(item,page){
        //Show images
        if(page == 0){
            $("#gallery_albums_holder").children().each(function(){$(this).remove()});
            $("#lightgallery").children().each(function(){$(this).remove()});
        }
        $.ajax({
            url : "API/getImages.php",
            method : "POST",
            data : {year:currentYear, album:item, page:page},
            success:function(data){
                var json = JSON.parse(data);
                if(json.count-page*20>20){
                    $("#nextPage").show();
                }else{
                    $("#nextPage").hide();
                }
                currentAlbum = item;
                json.images.forEach(function(item){
                    $(`
                    <div class="col-6 col-sm-4 col-md-3" data-src="gallery_images/`+item.rok+`/`+item.album+`/`+item.name+`.`+item.filetype+`">
                        <a href="">
                            <img class="galerieImage" src="gallery_images/`+item.rok+`/`+item.album+`/thumbnail/`+item.name+`.`+item.filetype+`"/>
                        </a>
                    </div>
                    `).appendTo("#lightgallery");
                });
                if(json.length == 0){
                    $('<p>Žádné obrázky</p>').appendTo("#gallery_albums_holder");
                }
                lightGallery(document.getElementById('lightgallery')); 
            }
        })
    }
    
    function onYearClicked(item){
        //Show albums
        document.getElementById("btnBack").style.display="block";
        $("#gallery_albums_holder").children().each(function(){$(this).remove()});
        $("#lightgallery").children().each(function(){$(this).remove()});
        $.ajax({
            url : "API/getAlbums.php",
            method : "POST",
            data : {year:String(item)},
            success:function(data){
                var json = JSON.parse(data);
                currentYear = item;
                json.forEach(function(item){
                    $(`
                    <div class="col-6 col-sm-4 col-md-3" >
                        <a href="#" onclick="onAlbumClicked('`+item.album+`',0);">
                            <img src="`+item.path+`" class="img-thumbnail galerieAlbumRok" alt="">
                            <p style="text-align:center;margin-bottom:2rem;color:#4a4a4a;">`+item.album+`</p>
                        </a>
                    </div>
                    `).appendTo("#gallery_albums_holder");
                });
                if(json.length == 0){
                    $('<p>Žádná alba</p>').appendTo("#gallery_albums_holder");
                }
            }
        })
    }
  
    function onGallery(){
        //Show onGallery
        document.getElementById("btnBack").style.display="none";
        $("#gallery_albums_holder").children().each(function(){$(this).remove()});
        $.ajax({
            url : "API/getGallery.php",
            method : "POST",
            data : {},
            success:function(data){
                var json = JSON.parse(data);
                json.forEach(function(item){
                    $(`
                    <div class="col-6 col-sm-4 col-md-3" >
                        <a href="#" onclick="onYearClicked('`+String(item.rok)+`');">
                            <img src="`+item.path+`"  class="img-thumbnail galerieAlbumRok" alt="">
                            <p style="text-align:center;margin-bottom:2rem;color:#4a4a4a;">`+item.rok+`</p>
                        </a>
                    </div>
                    `).appendTo("#gallery_albums_holder");
                });
                if(json.length == 0){
                    $('<p>Žádná alba</p>').appendTo("#gallery_albums_holder");
                }
            }
        })
    }
    onGallery();
</script>
</html>