<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Světlušky</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/svetlusky.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Světlušky</b><br><br>
                        <p class="pageData">
                            Do světlušek chodí holky ve věku od 2. (od 1. pokud dítě dříve chodilo 2 roky do benjamínků) do 5. třídy <br>
                            <br> Schůzky jsou každý týden během roku kromě prázdnin. Na schůzkách se děti učí rozvíjet spolupráci, fantazii, vztahy se svými 
                            vrstevníky... Během roku se koná i několik výprav.  O letních prázdninách se jezdí s ostatními družinami na 14-ti denní tábor. <br>
                            <br> Pro případné přihlášení dětí kontaktujte jednoho z rádců nebo přijďte na naši schůzku. <br><br> 
                            <b class="schuzkyDruziny">Schůzky družiny</b><br>Úterý 17:00 - 18:30 <br>
                            Schůzky začínají před <a class="lokalitaSchuzek" href="naseKlubovny">naší klubovnou</a>, nacházející se v 1. patře hasičky <br><br>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center"><b class="nazevDruziny">Vedoucí družiny</b></p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/lada.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Lada Černá</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 723 646 301 <br>
                                    e-mail: ladacerna16@seznam.cz
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/tana.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Taťána Dufková</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 605 117 225 <br>
                                    e-mail: tatanadufkova@seznam.cz
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>