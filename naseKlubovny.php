<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <link rel="stylesheet" href="public/css/carousel.css" />
    <title>Skaut Domašov | Kontakt</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <p class="nazev_klubovny">Domašovská klubovna</p>
                <div class="row">
                    <div class="col-md-6" >
                        <p>Naše domašovská základna, svépomocí a laskavostí obce Domašov vybudovaná, se nachází v podkroví hasičky v Domašově u 
                            rybníčku pod kostelem. V zimním období a nepříznivém počasí ji využíváme pro pravidelné schůzky. <br><br>Hlavní místností 
                            je klubovna s židlemi, stoly a dostatečným prostorem pro realizaci různorodých programů. Je možné si zde zatopit v krbu 
                            na dřevo (např. ve velkých mrazech jako podporu plynových kamen). <br><br>Součástí základny je i vybavená kuchyňka. Najdete v 
                            ní běžné nádobí: příbory, skleničky, hrnky, talíře, hrnce, varnou plotýnku, rychlovarnou konvici, apod. Lze v ní tedy bez 
                            problémů vařit. <br><br>Rádi u nás přivítáme zejména skautské oddíly či skupiny a klubovnu půjčíme, např. na vícedenní výpravy 
                            do našeho kraje a okolí. Pro pohodlnější přespání jsou dostupné staré matrace (pro cca 7 osob). <br><br> Na základě 30 let 
                            zkušeností užívání je limit klubovny maximálně 15 osob, kdy už je prostředí trochu stísněné. <br>
                            Kontakt: Mirek Staněk, skaut.domasov@seznam.cz
                        </p>
                    </div>
                    <div class="col-md-6">
                        <iframe style="border:none" src="https://en.frame.mapy.cz/s/govuvekese" width="100%" height="450px" frameborder="0"></iframe>                       
                    </div>
                </div>
                <div style="width:80%;margin:auto;padding-bottom:4rem;">
                    <div class="slideshow-container fade">
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna1.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna2.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna3.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna4.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna5.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna6.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna7.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/klubovna_domasov/klubovna8.jpg" style="width:100%">
                        </div>
                        <a class="Back" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="forward" onclick="plusSlides(1)">&#10095;</a>
                    </div><br>

                    <div style="text-align:center">
                        <span class="dots" onclick="currentSlide(1)"></span>
                        <span class="dots" onclick="currentSlide(2)"></span>
                        <span class="dots" onclick="currentSlide(3)"></span>
                        <span class="dots" onclick="currentSlide(4)"></span>
                        <span class="dots" onclick="currentSlide(5)"></span>
                        <span class="dots" onclick="currentSlide(6)"></span>
                        <span class="dots" onclick="currentSlide(7)"></span>
                        <span class="dots" onclick="currentSlide(8)"></span>
                    </div> 
                </div>
                <p style="font-family: Arial, Helvetica, sans-serif;font-size:1.5rem;margin-top:1.5rem;margin-bottom:0.5rem;">Ostatní klubovny</p>
                <p>
                    Pro více informací o ostatních klubovnách můžete zjistit na odkazech níže. <br>
                    <a href="https://skaut-rajhrad.webnode.cz/kontakt/">Klubovna v Rajhradě</a> - nepůjčuje se.<br>
                    <a href="https://tetcice.skauting.cz/kontakt/">Klubovna v Tetčicích</a> - kontakt: Aleš Staněk, skaut.tetcice@skaut.cz.<br>
                    <a href="https://zbysov.skauting.cz/kontakt/">Klubovna ve Zbýšově</a> - aktuálně v rekonstrukci, dokončení snad někdy během roku 2022.<br><br>
                </p>  
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
    <script>
        var slidePosition = 1;
        SlideShow(slidePosition);
        function plusSlides(n) {
            SlideShow(slidePosition += n);
        }
        function currentSlide(n) {
            SlideShow(slidePosition = n);
        }
        function SlideShow(n) {
            var i;
            var slides = document.getElementsByClassName("Containers");
            var circles = document.getElementsByClassName("dots");
            if (n > slides.length) {slidePosition = 1}
            if (n < 1) {slidePosition = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < circles.length; i++) {
                circles[i].className = circles[i].className.replace(" enable", "");
            }
            slides[slidePosition-1].style.display = "block";
            circles[slidePosition-1].className += " enable";
        } 
    </script>    
  </body>
</html>