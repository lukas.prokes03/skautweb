<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Skautky</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/skautky.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Skautky a Rangers</b><br><br>
                        <p class="pageData">
                            Naše schůzky jsou pro dívky navštěvující 6.-9. třídu základní školy nebo pro studentky gymnázia či střední školy. <br><br> 
                            Na schůzkách se věnujeme nejrůznějším aktivitám týkajících se skautingu, ale i aktivitám jako například pomoc v okolí 
                            ( roznášení betlemského světla, výroba vánočních přáníček,...), vaření, hry, apod.<br>
                            Během roku také podnikáme výpravy do přírody a v létě se společně s ostatními oddíly účastníme letního tábora. <br><br> 
                            Pro případné přihlášení kontaktujte rádce, nebo přijďte na schůzku. <br><br> 
                            <b class="schuzkyDruziny">Schůzky družiny</b><br>Pátek 17:30 - 19:00 <br>
                            Schůzky začínají před <a class="lokalitaSchuzek" href="naseKlubovny">naší klubovnou</a>, nacházející se v 1. patře hasičky <br><br>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center"><b class="nazevDruziny">Vedoucí družiny</b></p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/kacka.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Kateřina Gloserová</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 732 755 146 <br>
                                    e-mail: KatkaGloserova1@seznam.cz
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>