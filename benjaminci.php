<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Benjamínci</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php")?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/benjaminci.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Benjamínci</b><br><br>
                        <p class="pageData">Rády u nás přivítáme děti ve věku pěti a šesti let (předškoláky a žáky první třídy ZŠ). <br><br> Na schůzkách 
                            se plánujeme věnovat rozšiřování jejich schopností, práci ve skupině a poznávání přírody. Také připravujeme do našeho programu 
                            zařadit několik výprav. <br><br> V případě zájmu kontaktujte prosím jednu z vedoucích. <br><br> 
                            <b class="schuzkyDruziny">Schůzky družiny</b><br>Pátek 15:30 - 17:00 <br>
                            Schůzky začínají před <a class="lokalitaSchuzek" href="naseKlubovny">naší klubovnou</a>, nacházející se v 1. patře hasičky <br><br>
                        </p>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center">
                                    <b class="nazevDruziny">Vedoucí družiny</b>
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/petra.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Petra Valová</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 721 419 303 <br>
                                    e-mail: petavalova@email.cz
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/martinka.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Martina Řezaninová</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 123 456 789 <br>
                                    e-mail: email@gmail.com
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>