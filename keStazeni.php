<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Ke stažení</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/prihlaska_tabor_2021.doc"> 
                    <p style="margin-top:1rem"><img src="../public/img/icons/doc.png" width="60px"  alt="" > Přihláška Tábor 2021 Doc</p>
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/prihlaska_tabor_2021.pdf"> 
                    <p style="margin-top:1rem"><img src="../public/img/icons/pdf.png" width="60px" alt="" > Přihláška Tábor 2021 </p>
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/seznam_veci_tabor_2020.pdf"> 
                    <p style="margin-top:1rem"><img src="../public/img/icons/pdf.png" width="60px"  alt="" > Seznam věcí na tábor 2020 </p>
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/zdravotni_zpusobilost_ditete.pdf"> 
                    <p style="margin-top:1rem"><img src="../public/img/icons/pdf.png" width="60px" alt="" > Prohlášení o zdravotní způsobilosti dítěte </p>
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/prohlaseni_o_bezinfekcnosti_covid-19.pdf"> 
                    <p style="margin-top:1rem"><img src="../public/img/icons/pdf.png" width="60px" alt="" > Prohlášení o bezinfekčnosti 2020 </p>
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/prihlaska_skauti_domasov.pdf"> 
                    <p><img src="../public/img/icons/pdf.png" width="60px" alt="" > Přihláška oddíl GDPR </p> 
                </a>

                <a target="_blank" style="color:#707070" class="keStazeniNazev" href="../public/pdf/discipliny_zvas_2018.pdf"> 
                    <p style="margin-bottom:1rem;padding-bottom:3rem"><img src="../public/img/icons/pdf.png" width="60px" alt="" > Disciplíny ZVaS </p>
                </a>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div> 
  </body>
</html>
