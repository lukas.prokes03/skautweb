<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Skauti</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/vlcata.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Skauti</b><br><br>
                        <p class="pageData">
                            Skautská družina je dobrá parta kluků od 6. třídy, přibližně ve věku od 11 do 16 let. Na každotýdenních 
                            schůzkách během školního roku se snažíme zlepšovat jak v kamarádství, tak v různorodých dovednostech, od vyrábění přes 
                            vaření až po první pomoc. Rádi chodíme do přírody, pokud to aspoň trochu jde, protože se v ní můžeme nejenom vyblbnout, 
                            ale taky si zahrát pořádnou hru a trochu pohnout vlastním tělem. Kromě toho klademe důraz na kamarádství a vzájemnou pomoc.
                            <br><br> Každý měsíc se snažíme uspořádat fajnovou výpravu - ať už pěšky, na kole, na víkend na chatu nebo půjčenou klubovnu,
                             nebo úplně něco jiného (několikrát jsme jeli do Brna na Lasergame, do Jumpparku nebo třeba do Vidy). <br><br> 
                             Vrcholem našeho programu je čtrnáctidenní stanový letní tábor, kde se věnujeme různorodým skautským činnostem, jako je např. 
                             rozdělávání ohně, uzlování, šifrování, stopování, poznávání přírodnin a pobytu v přírodě. Mnohem důležitější ale je, že
                             můžeme být jako parta kamarádů spolu - právě tady totiž vznikají bez nadsázky pravá přátelství na celý život.<br><br> 
                             Pokud se chceš přidat, prostě dojdi na nějakou naši schůzku (když dáš předem vědět rádci družiny, tak to bude super).<br><br> 
                            <b class="schuzkyDruziny">Schůzky družiny</b><br>Středa 17:15 - 18:45 <br>
                            Začínáme před <a class="lokalitaSchuzek" href="naseKlubovny">naší klubovnou v Domašově</a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center"><b class="nazevDruziny">Vedoucí družiny</b></p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/vojta.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Vojta Staněk</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 601 507 845 <br>
                                    e-mail: vstanek@skaut.cz
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>