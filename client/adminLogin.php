<?php 
include ("../imports/client.php");
if (isLogged()){
    echo '<script>window.location.replace("../admin/adminIndex");</script>';
}
?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("../imports/head.php") ?>
    <title>Skaut Domašov</title>
    <style>
        @media only screen and (max-width:1023px){
            html{
                background-color: #FEF9F1 !important;
            }
            body{
                background-color: #FEF9F1 !important;
            }
        }
    </style>
  </head>
  <body>
    <?php include("../imports/body.php") ?>
    <div class="page">
        <div class="register">
            <p id="zrusit" style="text-align: left;display:none;position: absolute;margin-top: 10px;margin-left: 10px;">
                <i onclick="hideReset();" class="fas fa-times iconAdd" style="cursor:pointer"></i>
            </p> 
            <img src="../public/img/SKAUT_logo.png" class="logo_header" alt="logo" style="display: block;margin-left: auto;margin-right: auto;margin-top:4rem;width:20%;padding-top:3rem">
            <div style="width:48%;margin:auto;margin-top:3rem">
                <form  method="POST" id="loginForm" action="./loginCheck.php">
                    <p class="registerForm">Uživatelské jméno / E-mail:</p>
                    <input class="registerForm registerInput" style="margin-bottom:1rem;" type="text" id="username" name="username" required>
                    <p class="registerForm">Heslo:</p>
                    <input class="registerForm registerInput" type="password" id="password" name="password" placeholder="" required>

                    <a href="#" onclick="showReset()">Zapomněli jste heslo</a>
                    <p style="text-align:center;margin-top:1.5rem;padding-bottom:1.5rem;" class=" registerForm">
                    <input type="submit" name="submit" value="Přihlásit se"></p>
                </form>

                <form action="../API/resetPassword.php" id="resetForm" method="POST" style="display:none;">
                    <p class="registerForm">Zaregistrovaný e-mail:</p>
                    <p class="registerForm" > <input class="registerInput" type="email" name="email" placeholder="" required></p>
                    <p style="text-align: center;margin-top:0.5rem">Po potvrzení vyčkejte na ověřovací email</p>
                    <p style="text-align:center;margin-top:1.5rem;padding-bottom:1.5rem;" class=" registerForm"><input type="submit" name="odeslano" value="Potvrdit"></p>
                </form> 
            </div>
        </div>
    </div>
    <script>
        function showReset(){
            document.getElementById("loginForm").style.display = "none";
            document.getElementById("resetForm").style.display = "block";
            document.getElementById("zrusit").style.display = "block";
        }
        function hideReset(){
            document.getElementById("loginForm").style.display = "block";
            document.getElementById("resetForm").style.display = "none";
            document.getElementById("zrusit").style.display = "none";
        }
    </script>
  </body>
</html>
