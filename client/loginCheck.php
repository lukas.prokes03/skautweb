<?php 
session_start();
include("../admin/config.php");

if ( isset( $_POST['username'] ) && isset( $_POST['password']) ){
    $con = new mysqli($host, $user, $psw, $db);

    if (strpos($_POST["username"],"@") !== false){
        $stmt = $con->prepare("SELECT * FROM users WHERE email = ?");
        $stmt->bind_param('s', $_POST['username']);
        $stmt->execute();
    }else{
        $stmt = $con->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->bind_param('s', $_POST['username']);
        $stmt->execute();
    }

    $result = $stmt->get_result();
    $user = $result->fetch_object();
    if (password_verify( $_POST['password'] , $user->password )  ){ 
        $_SESSION['user_id'] = $user->id;
        $_SESSION['username'] = $user->username;
        $_SESSION['email'] = $user->email;
        $_SESSION['firstname'] = $user->firstname;
        $_SESSION['lastname'] = $user->lastname;
        $_SESSION['role'] = $user->role;
        echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"../admin/adminIndex\");</script>";
    }else{
        echo "<script type=\"text/javascript\" language=\"javascript\">window.location.replace(\"./adminLogin\");</script>";
    }
}
?>