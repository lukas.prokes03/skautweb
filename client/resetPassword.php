<?php 
include ("../imports/client.php");
?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("../imports/head.php") ?>
    <title>Skaut Domašov</title>
    <style>
        @media only screen and (max-width:1023px){
            html{
                background-color: #FEF9F1 !important;
            }
            body{
                background-color: #FEF9F1 !important;
            }
        }
    </style>
  </head>
  <body>
    <?php include("../imports/body.php") ?>
    <div class="page">
        <div class="register">
            <img src="../public/img/SKAUT_logo.png" class="logo_header" alt="logo" style="display: block;margin-left: auto;margin-right: auto;margin-top:4rem;width:20%;padding-top:3rem">
            <div style="width:48%;margin:auto;margin-top:3rem">
                <form  method="POST" id="loginForm" action="../API/resetPassword.php">
                    <input type="text" value='<?php echo $_GET["resetHash"]; ?>' name="resetHash" hidden >
                    <input type="text" value='<?php echo $_GET["email"]; ?>' name="email" hidden >
                    <p class="registerForm">Heslo:</p>
                    <input class="registerForm registerInput" style="margin-bottom:1rem;" type="password" id="password" name="password" required>
                    <p class="registerForm">Znovu heslo:</p>
                    <input class="registerForm registerInput" type="password" id="password2" name="password2" placeholder="" required>
                    <span id='zprava'></span>
                    <p style="text-align:center;margin-top:1.5rem;padding-bottom:1.5rem;" class=" registerForm">
                    <input type="submit" name="zmena" id="submitZmena" value="Změnit heslo"></p>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('#password, #password2').on('keyup', function () {
            if ($('#password').val() == $('#password2').val()) {
                $('#zprava').html('Hesla se shodují').css('color', 'green');
                document.getElementById("submitZmena").removeAttribute('disabled','disabled');
            } else {
                $('#zprava').html('Hesla se neshodují').css('color', 'red');
                document.getElementById("submitZmena").setAttribute('disabled','disabled');
            }   
        });
    </script>
  </body>
</html>
