-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 22. dub 2022, 15:46
-- Verze serveru: 10.4.14-MariaDB
-- Verze PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `skautweb`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `createdOn` datetime NOT NULL,
  `lastChange` datetime NOT NULL,
  `author` varchar(50) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `poradi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `createdOn`, `lastChange`, `author`, `enable`, `poradi`) VALUES
(1, 'Vánoční schůzka', '<p>Mil&iacute; br&aacute;&scaron;kov&eacute; a sestřičky!<img alt=\"\" height=\"226\" src=\"../public/img/article/ou8ab66l.jpg\" style=\"float:right\" width=\"300\" /></p>\r\n\r\n<p>V&aacute;noce se kvapně bl&iacute;ž&iacute;, a s nimi i na&scaron;e tradičn&iacute; v&aacute;nočn&iacute; schůzka. Ta se bude konat</p>\r\n\r\n<p><strong>4. adventn&iacute; neděli 22. 12. v 16:30.</strong></p>\r\n\r\n<p>Budeme r&aacute;di, když zase donesete nějak&eacute; dobroty, abychom si to mohli udělat hezk&eacute;. Nezapomeňte na d&aacute;rečky!</p>\r\n', '2022-04-22 01:17:52', '2022-04-22 01:23:42', '4', 1, 1),
(2, 'SkautFest', '<h3 style=\"text-align:center\">Srdečně v&aacute;s v&scaron;echny zveme na</h3>\r\n\r\n<h2 style=\"text-align:center\"><strong>SkautFest</strong></h2>\r\n\r\n<p>festival pln&yacute;&nbsp;<strong>skautingu, pohybu, vzděl&aacute;v&aacute;n&iacute;, uměn&iacute;, kultury, hudby, z&aacute;jmov&yacute;ch aktivit, cestov&aacute;n&iacute;,</strong>&nbsp;d&iacute;ky spolupr&aacute;ci s SDH Doma&scaron;ov i&nbsp;<strong>vystaven&eacute; hasičsk&eacute; techniky, hasičsk&eacute;ho z&aacute;sahu</strong>&nbsp;a mnoho dal&scaron;&iacute;ho, kter&yacute; proběhne&nbsp;<strong>18. 6. 2022</strong>. V&iacute;ce informac&iacute; najdete na&nbsp;<a href=\"http://www.skaut-domasov.cz/skautfest\" target=\"_blank\">str&aacute;nk&aacute;ch akce www.skaut-domasov.cz/skautfest</a>.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"191\" src=\"../public/img/article/podym7b2.jpg\" width=\"796\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2022-04-22 01:18:48', '2022-04-22 01:23:07', '4', 1, 2),
(3, 'Letní tábor 2022', '<p>Leto&scaron;n&iacute; letn&iacute; t&aacute;bor pro vlčata, světlu&scaron;ky, skauty a skautky pod Templ&scaron;tejnem u Jihlavky&nbsp;<strong>proběhne</strong>&nbsp;od<strong>&nbsp;2.&nbsp;7.&nbsp;</strong>do&nbsp;<strong>16.&nbsp;7.&nbsp;2022</strong>. Přihl&aacute;&scaron;ky můžete st&aacute;nhnout v sekci Ke stažen&iacute;, najdete v nich i podrobněj&scaron;&iacute; informace. Vyplněn&eacute; přihl&aacute;&scaron;ky odevzd&aacute;vejte nejpozději do 31. března 2022.</p>\r\n\r\n<p>T&eacute;matem t&aacute;bora bude Galsk&aacute; osada, tak jak ji zn&aacute;me z filmů o Asterixovi a Obelixovi, kteř&iacute; s n&aacute;mi na t&aacute;boře možn&aacute; tak&eacute; budou. Abychom jim nedělali ostudu, budeme potřebovat galsk&yacute; kost&yacute;m. Inspirovat se můžete pr&aacute;vě s filmů o Asterixovi a Obelixovi. Obecně stač&iacute; historizuj&iacute;c&iacute; (<em>tj. takov&eacute;, kter&eacute; nevypad&aacute; př&iacute;li&scaron; moderně)</em>&nbsp;oblečen&iacute; - např. přep&aacute;san&aacute; tunika, př&iacute;padně hrub&aacute; ko&scaron;ile či jednobarevn&eacute; tričko, nemus&iacute; to b&yacute;t nic profesion&aacute;ln&iacute;ho.</p>\r\n\r\n<p>Vzhledem ke koronavirov&eacute; situaci nejsou zn&aacute;my přesn&eacute; podm&iacute;nky pro kon&aacute;n&iacute; t&aacute;bora. Nav&iacute;c je o n&aacute;&scaron; t&aacute;bor každ&yacute; rok vět&scaron;&iacute; z&aacute;jem. Je tedy možn&eacute;, že bude omezen maxim&aacute;ln&iacute; možn&yacute; počet &uacute;častn&iacute;ků. Aktu&aacute;ln&iacute; limit je 29 dět&iacute; do 15 let. Přednost maj&iacute; registrovan&iacute; členov&eacute;, kteř&iacute; pravidelně chod&iacute; na schůzky a na v&yacute;pravy. Pokud by zůstalo nějak&eacute; m&iacute;sto voln&eacute;, můžeme po dohodě př&iacute;padně někoho přibrat. &Uacute;častn&iacute;ci budou vyb&iacute;r&aacute;ni na z&aacute;kladě pořad&iacute; odevzdan&eacute; přihl&aacute;&scaron;ky (na&scaron;e heslo:&nbsp;<em>Co může&scaron; udělat dnes, neodkl&aacute;dej na z&iacute;třek</em>), proto s pod&aacute;n&iacute;m přihl&aacute;&scaron;ky neot&aacute;lejte.</p>\r\n\r\n<p>Tě&scaron;&iacute;me se na v&aacute;s!</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"397\" src=\"../public/img/article/kzkmtade.jpg\" width=\"701\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2022-04-22 01:21:06', '2022-04-22 01:21:06', '4', 1, 3),
(4, 'Kapacita tábora 2022 naplněna!', '<p>Moc n&aacute;s tě&scaron;&iacute; z&aacute;jem o t&aacute;bor! Př&iacute;pravy jsou v pln&eacute;m proudu a už se nemůžeme dočkat, až konečně pojedeme.</p>\r\n\r\n<p>Aktu&aacute;lně m&aacute;me na leto&scaron;n&iacute; t&aacute;bor přihl&aacute;&scaron;en&yacute;ch 29 dět&iacute; do patn&aacute;cti let. To znamen&aacute;, že dal&scaron;&iacute; připadn&eacute; přihl&aacute;&scaron;en&eacute; už budeme evidovat jako n&aacute;hradn&iacute;ky pro př&iacute;pad, že by se někdo z přihl&aacute;&scaron;en&yacute;ch nemohl z&uacute;časnit.</p>\r\n', '2022-04-22 01:24:14', '2022-04-22 12:21:19', '4', 1, 4),
(5, 'Víkendovka vlčat v Boskovicích', '<p style=\"text-align:center\"><img alt=\"\" height=\"961\" src=\"../public/img/article/iidsv301.jpg\" width=\"681\" /></p>\r\n', '2022-04-22 01:25:29', '2022-04-22 14:23:49', '4', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `gallerythumbnails`
--

CREATE TABLE `gallerythumbnails` (
  `id` int(11) NOT NULL,
  `rok` varchar(4) NOT NULL,
  `album` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `gallerythumbnails`
--

INSERT INTO `gallerythumbnails` (`id`, `rok`, `album`, `path`) VALUES
(1, '2022', '', '../gallery_thumbnails/years/myvx5uyx.jpg'),
(2, '2022', 'Tábor', '../gallery_thumbnails/albums/0u10ihmg.jpg'),
(3, '2022', 'Výprava do Trenckovy rokle', '../gallery_thumbnails/albums/khjohmyu.jpg'),
(4, '2021', '', '../gallery_thumbnails/years/jdmgy01v.jpg'),
(5, '2021', 'Výprava Ivančena', '../gallery_thumbnails/albums/tvif3xs2.jpg'),
(6, '2021', 'Výprava Kralický Sněžník', '../gallery_thumbnails/albums/5jpnrxlj.jpg'),
(7, '2020', '', '../gallery_thumbnails/years/6563k8yy.jpg'),
(8, '2020', 'Tábor', '../gallery_thumbnails/albums/89xm4k9t.jpg'),
(9, '2022', 'Zimní výprava Šumava', '../gallery_thumbnails/albums/xzrxjcf3.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabulky `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `rok` varchar(4) NOT NULL,
  `album` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `filetype` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `images`
--

INSERT INTO `images` (`id`, `rok`, `album`, `name`, `filetype`) VALUES
(1, '2022', 'Tábor', 'jzzkou1k', 'jpg'),
(2, '2022', 'Tábor', 'g6ipycac', 'jpg'),
(3, '2022', 'Tábor', 'p2sohug8', 'jpg'),
(4, '2022', 'Tábor', '0di1prfm', 'jpg'),
(5, '2022', 'Tábor', 'yt369nzh', 'jpg'),
(6, '2022', 'Tábor', 'yb53v3u4', 'jpg'),
(7, '2022', 'Tábor', 'hqh6qzs9', 'jpg'),
(8, '2022', 'Tábor', 'zw2rqbgq', 'jpg'),
(9, '2022', 'Tábor', 't0xcl4xs', 'jpg'),
(10, '2022', 'Tábor', 'i20358d5', 'jpg'),
(11, '2022', 'Tábor', '9pzwd79v', 'jpg'),
(12, '2022', 'Tábor', 'tjip67gz', 'jpg'),
(13, '2022', 'Tábor', '30legh0h', 'jpg'),
(14, '2022', 'Tábor', 'lp39wdfn', 'jpg'),
(15, '2022', 'Tábor', '5dukqea8', 'jpg'),
(16, '2022', 'Tábor', '3d6whny5', 'jpg'),
(17, '2022', 'Tábor', 'liygjngo', 'jpg'),
(18, '2022', 'Tábor', 'm07dt8o3', 'jpg'),
(19, '2022', 'Tábor', 'im0numm8', 'jpg'),
(20, '2022', 'Tábor', '8w3ltzma', 'jpg'),
(21, '2022', 'Tábor', 'cghpsg3r', 'jpg'),
(22, '2022', 'Tábor', '4n60id4b', 'jpg'),
(23, '2022', 'Tábor', 'fwswh9vu', 'jpg'),
(24, '2022', 'Tábor', 'fb3t8air', 'jpg'),
(25, '2022', 'Tábor', '052vzgup', 'jpg'),
(26, '2022', 'Tábor', '4gm9ikfy', 'jpg'),
(27, '2022', 'Výprava do Trenckovy rokle', 'cna94g5m', 'jpg'),
(28, '2022', 'Výprava do Trenckovy rokle', 'oe42bfwa', 'jpg'),
(29, '2022', 'Výprava do Trenckovy rokle', '364rpili', 'jpg'),
(30, '2022', 'Výprava do Trenckovy rokle', 'fu3nhalm', 'jpg'),
(31, '2022', 'Výprava do Trenckovy rokle', 'l4jm7q9m', 'jpg'),
(32, '2022', 'Výprava do Trenckovy rokle', 'drt5sl1a', 'jpg'),
(33, '2022', 'Výprava do Trenckovy rokle', 'sz53vddz', 'jpg'),
(34, '2022', 'Výprava do Trenckovy rokle', 'lvfnd87e', 'jpg'),
(35, '2022', 'Výprava do Trenckovy rokle', '3p0lor7t', 'jpg'),
(36, '2022', 'Výprava do Trenckovy rokle', '2iycw4b8', 'jpg'),
(37, '2022', 'Výprava do Trenckovy rokle', 'zrt92gxl', 'jpg'),
(38, '2022', 'Výprava do Trenckovy rokle', '18vb3p9l', 'jpg'),
(39, '2022', 'Výprava do Trenckovy rokle', 'xb93sx5i', 'jpg'),
(40, '2022', 'Výprava do Trenckovy rokle', '4op336g4', 'jpg'),
(41, '2022', 'Výprava do Trenckovy rokle', 'nkojt45t', 'jpg'),
(42, '2022', 'Výprava do Trenckovy rokle', 'oafv3kcv', 'jpg'),
(43, '2021', 'Výprava Ivančena', 'txin3jml', 'jpg'),
(44, '2021', 'Výprava Ivančena', 'vylgebbj', 'jpg'),
(45, '2021', 'Výprava Ivančena', 'h3t50h9m', 'jpg'),
(46, '2021', 'Výprava Ivančena', 'g5gfxf51', 'jpg'),
(47, '2021', 'Výprava Ivančena', '1d6d8zs3', 'jpg'),
(48, '2021', 'Výprava Ivančena', 'olw7uewc', 'jpg'),
(49, '2021', 'Výprava Ivančena', 'v2vsiod7', 'jpg'),
(50, '2021', 'Výprava Ivančena', 'b137icq2', 'jpg'),
(51, '2021', 'Výprava Ivančena', 'atnbtp4i', 'jpg'),
(52, '2021', 'Výprava Ivančena', 'z6gzh8yy', 'jpg'),
(53, '2021', 'Výprava Ivančena', 'ptiju3em', 'jpg'),
(54, '2021', 'Výprava Ivančena', 'a0jcoz5m', 'jpg'),
(55, '2021', 'Výprava Ivančena', 'sf2dlr2q', 'jpg'),
(56, '2021', 'Výprava Ivančena', 't77fja1d', 'jpg'),
(57, '2021', 'Výprava Ivančena', 'sti6px9j', 'jpg'),
(58, '2021', 'Výprava Ivančena', '0vvvasrc', 'jpg'),
(59, '2021', 'Výprava Ivančena', 'kaj2yh5x', 'jpg'),
(60, '2021', 'Výprava Ivančena', '5t9jwxam', 'jpg'),
(61, '2021', 'Výprava Ivančena', 'lxczxugm', 'jpg'),
(62, '2021', 'Výprava Ivančena', '9guafoty', 'jpg'),
(63, '2021', 'Výprava Ivančena', 'c9m3vrgg', 'jpg'),
(64, '2021', 'Výprava Ivančena', 'mm60r4tr', 'jpg'),
(65, '2021', 'Výprava Ivančena', '90y9icbw', 'jpg'),
(66, '2021', 'Výprava Ivančena', '21715xza', 'jpg'),
(67, '2021', 'Výprava Ivančena', '0larkq9s', 'jpg'),
(68, '2021', 'Výprava Ivančena', 'bjldb0td', 'jpg'),
(69, '2021', 'Výprava Ivančena', 'u8pg43hm', 'jpg'),
(70, '2021', 'Výprava Ivančena', 'al3mjmlw', 'jpg'),
(71, '2021', 'Výprava Ivančena', 'r3qbjsd1', 'jpg'),
(72, '2021', 'Výprava Ivančena', 'koh16epb', 'jpg'),
(73, '2021', 'Výprava Ivančena', 'b6do5oyb', 'jpg'),
(74, '2021', 'Výprava Ivančena', 'uw7craph', 'jpg'),
(75, '2021', 'Výprava Ivančena', 'bfdimgdu', 'jpg'),
(76, '2021', 'Výprava Ivančena', 'i6y6k05g', 'jpg'),
(77, '2021', 'Výprava Kralický Sněžník', 'tidhxgsf', 'jpg'),
(78, '2021', 'Výprava Kralický Sněžník', '18549rni', 'jpg'),
(79, '2021', 'Výprava Kralický Sněžník', 'xlidvgf7', 'jpg'),
(80, '2021', 'Výprava Kralický Sněžník', 'sqbdrwoz', 'jpg'),
(81, '2021', 'Výprava Kralický Sněžník', 'fm00w9mr', 'jpg'),
(82, '2021', 'Výprava Kralický Sněžník', '7gsuomef', 'jpg'),
(83, '2021', 'Výprava Kralický Sněžník', 'labx1rt6', 'jpg'),
(84, '2021', 'Výprava Kralický Sněžník', 'iwtk4koe', 'jpg'),
(85, '2021', 'Výprava Kralický Sněžník', 'pk5uq3fa', 'jpg'),
(86, '2021', 'Výprava Kralický Sněžník', 'lyhuqbut', 'jpg'),
(87, '2021', 'Výprava Kralický Sněžník', 'x3vnouc9', 'jpg'),
(88, '2021', 'Výprava Kralický Sněžník', 'q7ad8174', 'jpg'),
(89, '2021', 'Výprava Kralický Sněžník', 'ctuk01yc', 'jpg'),
(90, '2021', 'Výprava Kralický Sněžník', 'cgfzz9oq', 'jpg'),
(91, '2021', 'Výprava Kralický Sněžník', 'n56eqzj5', 'jpg'),
(92, '2021', 'Výprava Kralický Sněžník', 'sx1bjmby', 'jpg'),
(93, '2021', 'Výprava Kralický Sněžník', 'irlmpx13', 'jpg'),
(94, '2021', 'Výprava Kralický Sněžník', '3un7yua5', 'jpg'),
(95, '2021', 'Výprava Kralický Sněžník', 'd6gszjzy', 'jpg'),
(96, '2021', 'Výprava Kralický Sněžník', 'n2h9u1m7', 'jpg'),
(97, '2021', 'Výprava Kralický Sněžník', '15k125v1', 'jpg'),
(98, '2021', 'Výprava Kralický Sněžník', 'aos5jjc1', 'jpg'),
(99, '2021', 'Výprava Kralický Sněžník', '0000cupk', 'jpg'),
(100, '2021', 'Výprava Kralický Sněžník', 'hs93np1x', 'jpg'),
(101, '2021', 'Výprava Kralický Sněžník', 'eh4g45rg', 'jpg'),
(102, '2021', 'Výprava Kralický Sněžník', '2vkjj8fs', 'jpg'),
(103, '2021', 'Výprava Kralický Sněžník', '4i8ks6nw', 'jpg'),
(104, '2021', 'Výprava Kralický Sněžník', 'toz9dk5h', 'jpg'),
(105, '2021', 'Výprava Kralický Sněžník', 'hjkdt4y8', 'jpg'),
(106, '2021', 'Výprava Kralický Sněžník', '2s3e7fo6', 'jpg'),
(107, '2021', 'Výprava Kralický Sněžník', 'x8qxzhpl', 'jpg'),
(108, '2021', 'Výprava Kralický Sněžník', '6xxjjpcb', 'jpg'),
(109, '2021', 'Výprava Kralický Sněžník', 'ho0c30rn', 'jpg'),
(110, '2021', 'Výprava Kralický Sněžník', 'v9q1om7t', 'jpg'),
(111, '2021', 'Výprava Kralický Sněžník', 'p5mspkxy', 'jpg'),
(112, '2021', 'Výprava Kralický Sněžník', '9xsnkgto', 'jpg'),
(113, '2021', 'Výprava Kralický Sněžník', 'd2xz12nv', 'jpg'),
(114, '2021', 'Výprava Kralický Sněžník', 'bpwb5gay', 'jpg'),
(115, '2021', 'Výprava Kralický Sněžník', 'og43jxvn', 'jpg'),
(116, '2021', 'Výprava Kralický Sněžník', '6bqebfor', 'jpg'),
(117, '2021', 'Výprava Kralický Sněžník', 'tnqwptis', 'jpg'),
(118, '2020', 'Tábor', '8f3jeta4', 'jpg'),
(119, '2020', 'Tábor', '7daplc0u', 'jpg'),
(120, '2020', 'Tábor', '7lckowho', 'jpg'),
(121, '2020', 'Tábor', 'xl3t0uwn', 'jpg'),
(122, '2020', 'Tábor', '4ekuh1gh', 'jpg'),
(123, '2020', 'Tábor', '2ffyvefo', 'jpg'),
(124, '2020', 'Tábor', 'a6dzkj48', 'jpg'),
(125, '2020', 'Tábor', 'mbo17ex3', 'jpg'),
(126, '2020', 'Tábor', 'n9jt55b7', 'jpg'),
(127, '2020', 'Tábor', 'ht2jud8a', 'jpg'),
(128, '2020', 'Tábor', 't5qlhkrs', 'jpg'),
(129, '2020', 'Tábor', '5j9t6nrz', 'jpg'),
(130, '2020', 'Tábor', 'ny3qgcl1', 'jpg'),
(131, '2020', 'Tábor', 'bzl9rv6j', 'jpg'),
(132, '2020', 'Tábor', 'dkfr4xwd', 'jpg'),
(133, '2020', 'Tábor', '8legcg2e', 'jpg'),
(134, '2020', 'Tábor', 'pq9kzu2z', 'jpg'),
(135, '2020', 'Tábor', '7x00b4a9', 'jpg'),
(136, '2020', 'Tábor', 'pltsu7ek', 'jpg'),
(137, '2020', 'Tábor', '50pvrvaz', 'jpg'),
(138, '2020', 'Tábor', 'iiijyojq', 'jpg'),
(139, '2020', 'Tábor', 'vsgcxib0', 'jpg'),
(140, '2020', 'Tábor', '0qhrcgss', 'jpg'),
(141, '2020', 'Tábor', 'vnkjswfh', 'jpg'),
(142, '2020', 'Tábor', 'pspjk4kl', 'jpg'),
(143, '2020', 'Tábor', 'tp1hqcph', 'jpg'),
(144, '2020', 'Tábor', '4rvgppqf', 'jpg'),
(145, '2020', 'Tábor', 'isjkfqz4', 'jpg'),
(146, '2020', 'Tábor', 'p6cjvq8q', 'jpg'),
(147, '2020', 'Tábor', 'hcdun82t', 'jpg'),
(148, '2020', 'Tábor', '6b6aiobt', 'jpg'),
(149, '2020', 'Tábor', 'qoqml2e0', 'jpg'),
(150, '2020', 'Tábor', 'tkdsxwd1', 'jpg'),
(151, '2020', 'Tábor', '4d7crmsx', 'jpg'),
(152, '2020', 'Tábor', 'yjxhpf9y', 'jpg'),
(153, '2020', 'Tábor', '0xv5sk3j', 'jpg'),
(154, '2020', 'Tábor', 'g2998pxt', 'jpg'),
(155, '2020', 'Tábor', 'fug6n8m4', 'jpg'),
(156, '2020', 'Tábor', '1gj8wda0', 'jpg'),
(157, '2020', 'Tábor', '7puw0jys', 'jpg'),
(158, '2020', 'Tábor', 'dvkp067i', 'jpg'),
(159, '2020', 'Tábor', 'uvfozuof', 'jpg'),
(160, '2020', 'Tábor', 'qfrfncxj', 'jpg'),
(161, '2020', 'Tábor', 'crhvf45k', 'jpg'),
(162, '2020', 'Tábor', 'px5ejy6e', 'jpg'),
(163, '2020', 'Tábor', 'gpzh7rr6', 'jpg'),
(164, '2020', 'Tábor', 'xbuue19h', 'jpg'),
(165, '2020', 'Tábor', '0ynkfubp', 'jpg'),
(166, '2020', 'Tábor', '9n8ywvpi', 'jpg'),
(167, '2020', 'Tábor', 'u8s7mhlo', 'jpg'),
(168, '2020', 'Tábor', 'gjc6ixlv', 'jpg'),
(169, '2020', 'Tábor', 'm1eouz32', 'jpg'),
(170, '2020', 'Tábor', 'cbsoiirp', 'jpg'),
(171, '2020', 'Tábor', 'akwc1vcz', 'jpg'),
(172, '2020', 'Tábor', 'f29bck39', 'jpg'),
(173, '2020', 'Tábor', 'zakpzyfe', 'jpg'),
(174, '2020', 'Tábor', 'ieljlx38', 'jpg'),
(175, '2020', 'Tábor', '5u3ijytx', 'jpg'),
(176, '2020', 'Tábor', 'k99ilyze', 'jpg'),
(177, '2020', 'Tábor', 'ydamiewy', 'jpg'),
(178, '2020', 'Tábor', 'y73kkbvr', 'jpg'),
(179, '2020', 'Tábor', 'jrq9fgd4', 'jpg'),
(180, '2020', 'Tábor', '8qu7w5lm', 'jpg'),
(181, '2022', 'Zimní výprava Šumava', '0z7epdj3', 'jpeg'),
(182, '2022', 'Zimní výprava Šumava', '6dm0iqhg', 'jpeg'),
(183, '2022', 'Zimní výprava Šumava', 'r9kleag2', 'jpeg'),
(184, '2022', 'Zimní výprava Šumava', 'eiipaf09', 'jpeg'),
(185, '2022', 'Zimní výprava Šumava', '4t6dzkk3', 'jpeg'),
(186, '2022', 'Zimní výprava Šumava', 'mv5mqg3b', 'jpeg'),
(187, '2022', 'Zimní výprava Šumava', 'a23bn489', 'jpeg'),
(188, '2022', 'Zimní výprava Šumava', 'ger6yke6', 'jpeg'),
(189, '2022', 'Zimní výprava Šumava', '3f1uzypw', 'jpg'),
(190, '2022', 'Zimní výprava Šumava', 'awdqw2g8', 'jpeg'),
(191, '2022', 'Zimní výprava Šumava', 'jt15fpnv', 'jpeg'),
(192, '2022', 'Zimní výprava Šumava', 'rtcr135l', 'jpeg'),
(193, '2022', 'Zimní výprava Šumava', 'mvcusd5r', 'jpeg'),
(194, '2022', 'Zimní výprava Šumava', '0p3ftfwz', 'jpeg'),
(195, '2022', 'Zimní výprava Šumava', 'jnwqvl5q', 'jpeg'),
(196, '2022', 'Zimní výprava Šumava', '349sniw3', 'jpeg'),
(197, '2022', 'Zimní výprava Šumava', '0kf67ijt', 'jpeg'),
(198, '2022', 'Zimní výprava Šumava', 'wyg42uvy', 'jpeg'),
(199, '2022', 'Zimní výprava Šumava', 'u2jgms2m', 'jpeg'),
(200, '2022', 'Zimní výprava Šumava', '3nip1x39', 'jpeg'),
(201, '2022', 'Zimní výprava Šumava', 'fejvlw3x', 'jpeg');

-- --------------------------------------------------------

--
-- Struktura tabulky `schuzky`
--

CREATE TABLE `schuzky` (
  `id` int(11) NOT NULL,
  `oddil` varchar(30) NOT NULL,
  `den` varchar(10) NOT NULL,
  `denCely` varchar(30) NOT NULL,
  `zacatek` varchar(30) NOT NULL,
  `konec` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `schuzky`
--

INSERT INTO `schuzky` (`id`, `oddil`, `den`, `denCely`, `zacatek`, `konec`) VALUES
(1, 'skauti', 'ST', 'středa', '17:15', '18:45'),
(2, 'skautky', 'pá', 'pátek', '17:30', '19:00'),
(3, 'vlčata', 'PO', 'pondělí', '16:00', '17:30'),
(4, 'světlušky', 'út', 'úterý', '17:00', '18:30'),
(5, 'benjamínci', 'pá', 'pátek', '15:30', '17:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`) VALUES
(3, 'LukCZ@post.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `resetHash` varchar(16) DEFAULT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `resetHash`, `firstname`, `lastname`, `role`) VALUES
(1, 'root', 'root@skautdomasov.cz', '$2y$10$p.NMqVd2ln32uvQ4vaRtXu4IyIbbmhFnEbkqEVSq1fx8Sj97n9FcS', NULL, 'František', 'Novotný', 2),
(2, 'admin', 'admin@skautdomasov.cz', '$2y$10$F7IbAaMv.ffX6luxblAMBuo71UNM5kuB57coA01Iq4BZVvja04rY.', NULL, 'Petr', 'Svoboda', 1),
(3, 'uzivatel', 'uzivatel@skautdomasov.cz', '$2y$10$bz5WWL9jRqSnV6xMJAsjn.d7uVO51P8Mo7YaVfTZisovR4VmOiAry', NULL, 'Honza', 'Navratil', 0),
(4, 'Lukas', 'prokes.lukas@purkynka.cz', '$2y$10$pK3UpfiHDbuXlaKv45DeB.af2PKymk2LLt875/6ETWMmdqkEHzlmG', NULL, 'Lukáš', 'Prokeš', 2);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `gallerythumbnails`
--
ALTER TABLE `gallerythumbnails`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `schuzky`
--
ALTER TABLE `schuzky`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `gallerythumbnails`
--
ALTER TABLE `gallerythumbnails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pro tabulku `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT pro tabulku `schuzky`
--
ALTER TABLE `schuzky`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
