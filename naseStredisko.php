<?php include ("imports/client.php")?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Náš oddíl</title>
  </head>
  <body>
      <script>
            
      </script>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <img src="public/img/spol_fotka_tabor2.png" alt="" width="100%">
                <p style="font-family: Arial, Helvetica, sans-serif;font-size:1.5rem;margin-top:1.5rem;margin-bottom:0.5rem;">Středisko Devíti křížů Domašov</p>
                <p>
                    Píše se rok 1990, a po více než čtyřiceti letech totality, kdy skauting nemohl fungovat, zakládají Miroslav a 
                    Ivana Staňkovi dva oddíly skautů a skautek, kterým se snaží předávat myšlenky a výchovu v čestné a zodpovědné lidi.
                    <br><br>Později Staňkovi ustavují středisko Devíti křížů Domašov s právní subjektivitou. Název střediska připomíná kulturní 
                    památku <a href="https://cs.wikipedia.org/wiki/Dev%C4%9Bt_k%C5%99%C3%AD%C5%BE%C5%AF">Devět křížů</a>, což je místo, na kterém podle staré pověsti došlo v 16. století ke krvavému neštěstí svatebního průvodu. 
                    Zároveň se ale v názvu odráží zaměření střediska na rozšířenou duchovní a náboženskou výchovu.<br><br>Dnes v Domašově stále fungují 
                    dva oddíly - chlapecký a dívčí, avšak do střediska patří i oddíly v Rajhradě a Tetčicích, a aktivně pomáháme s obnovou oddílu ve 
                    Zbýšově. Středisko i oddíly patří do organizace JUNÁK - český skaut, z.s., který je dobrovolné, nezávislé a nepolitické občanské 
                    sdružení sdružující své členy a členky bez rozdílu národnosti, náboženského vyznání, politického přesvědčení, rasy nebo jiných rozdílů.<br><br>
                </p>
                <p style="font-family: Arial, Helvetica, sans-serif;font-size:1.5rem;margin-top:1.5rem;margin-bottom:0.5rem;">Dívčí oddíl</p>
                <p>
                    V dívčím oddíle jsou aktivní všechny věkové kategorie. Každý týden se na schůzky samostatně schází benjamínci, světlušky i skautky, 
                    pro které starší a zkušenější skautky a roverky připravují program. Kromě pravidelných setkání během roku nezahálíme, pořádáme 
                    výpravy, kulturní akce (jako je např. tradiční adventní vyrábění) apod. Vedoucím dívčího oddílu je Maria Staňková (Mapa).<br><br>
                </p>
                <p id="nadpis" style="font-family: Arial, Helvetica, sans-serif;font-size:1.5rem;margin-top:1.5rem;margin-bottom:0.5rem;">Chlapecký oddíl </p>
                <p>
                    V chlapeckém oddíle také rádi přivítáme kluky všeho věku, od benjamínků až po rovery. Pravidelné schůzky během školního roku mají 
                    benjamínci, vlčata a skauti, o něž se starají opět ti zkušenější, tj. starší skauti a roveři. Často uspořádáme nějakou výpravu nebo 
                    akci a snažíme se pomáhat i v našem okolí, například posekáním a úklidem farní zahrady nebo sázením stromů. Vedoucím chlapeckého 
                    oddílu je Mirek Staněk.<br><br>
                </p>
                <p style="font-family: Arial, Helvetica, sans-serif;font-size:1.5rem;margin-top:1.5rem;margin-bottom:0.5rem;">Ostatní oddíly</p>
                <p>
                    <a href="https://skaut-rajhrad.webnode.cz/">Koedukovaný oddíl Svatého Václava v Rajhradě</a><br>
                    <a href="https://tetcice.skauting.cz/">Koedukovaný oddíl v Tetčicích</a><br>
                    Obnovovaný <a href="https://zbysov.skauting.cz/">koedukovaný oddíl ve Zbýšově</a><br><br>
                </p>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
    <script>
        function isInViewPort(element) {
                var bounding = element.getBoundingClientRect();

                if (
                    bounding.top >= 0 &&
                    bounding.left >= 0 &&
                    bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
                    bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
                ) {
                    return true;
                } else {
                    return false;
                }
            }

        var elementt = document.getElementById("nadpis");

        window.addEventListener('scroll', function (event) {
           if(isInViewPort(elementt)){
            console.log("is in viewport");
           }
        }, false);



    </script>
  </body>
</html>