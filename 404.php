<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("./imports/head.php") ?>
    <title>Skaut Domašov</title>
    <style>
        @media only screen and (max-width:1023px){
            html{
                background-color: #FEF9F1 !important;
            }
            body{
                background-color: #FEF9F1 !important;
            }
        }
        .buttonBackDiv{
            display: flex;
            justify-content: center;
            margin-top: 2rem;
            padding-bottom: 4rem;
        }
        .buttonBack{
            background-color:#ffa200;
            color: white;
            font-size: 1rem;
        }

        .text404{
            text-align: center;
            line-height: 1;
            margin-bottom: 2rem;
            padding-top: 4rem;
            color: #ffa200;
            font-size: 9rem;
        }
        .main{
            margin-top: 18vh !important;
            margin: auto;
            background-color: var(--bg-color-light);
        }
        @media only screen and (max-width:620px) {
            .main{
                width:90%;
            } 
        }
        @media only screen and (min-width:620px) {
            .main{
                width:40%;
            } 
        }
    </style>
  </head>
  <body>
    <?php include("./imports/body.php") ?>
    <div class="main" style="">
        <p class="text404">404</p>
        <h2 style="text-align:center;">Stránka kterou hledáte neexistuje</h2>
        <div class="buttonBackDiv">
            <button type="button" class="btn btn-rounded buttonBack" onclick="window.history.back()">Zpět na předchozí stranu</button>
        </div>
    </div>
    
  </body>
</html>
