<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <title>Skaut Domašov | Kontakt</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-7">
                        <b class="kontaktyNadpis">Název střediska:</b><br>
                        <p class="kontaktyData pageData">Junák – český skaut, středisko Devíti křížů Domašov, z. s.</p><br>
                        
                        <b class="kontaktyNadpis">Evidenční číslo střediska v Junáku:</b><br>
                        <p class="kontaktyData pageData">623.26</p><br>

                        <b class="kontaktyNadpis">IČ střediska:</b><br>
                        <p class="kontaktyData pageData">49459953</p><br>

                        <b class="kontaktyNadpis">Bankovní účet střediska:</b><br>
                        <p class="kontaktyData pageData">1391051001/5500</p><br>

                        <b class="kontaktyNadpis">Korespondenční adresa střediska:</b><br>
                        <p class="kontaktyData pageData">Rudka 98, 66483 Domašov</p><br>

                        <b class="kontaktyNadpis">Vedoucí střediska:</b><br>
                        <p class="kontaktyData pageData">RNDr. Miroslav Staněk <br>
                        skaut.domasov@seznam.cz</p><br>

                        <b class="kontaktyNadpis">Oddíly ve středisku:</b><br>
                        <p class="kontaktyData pageData">
                            1. dívčí oddíl Domašov<br>
                            1. chlapecký oddíl Domašov - oba oddíly informace zde na webu<br>
                            1. koedukovaný oddíl Tetčice - https://sites.google.com/site/skauttetcice<br>
                            koedukovaný oddíl sv. Václava Rajhrad - http://skautrajhrad.webnode.cz
                        </p><br>
                    </div>
                    <div class="col-md-5">
                        <div class="blurBackground">
                            <p style="text-align:center">
                                <a href="naseKlubovny" style="color:#707070;"><b class="kontaktyKlubovna">Klubovna</b></a><br>
                            </p>
                            <p class="kontaktyData" style="margin-bottom:0.2rem"><b>Adresa:</b> Hasička V Dědině, 66483, Domašov</p>
                            <p class="kontaktyData" style="margin-bottom:0.2rem"><b>GPS:</b> 49.244558055556E 16.34716N</p>

                            <p style="text-align:center;margin-top:2rem;" >
                                <a href="taboristeJamolice" style="color:#707070;"><b class="kontaktyKlubovna">Tábořiště</b></a><br>
                            </p>
                            <p class="kontaktyData" style="margin-bottom:0.2rem">U Jamolic na břehu řeky Jihlavky pod Templštýnem</p>
                            <p class="kontaktyData" style="margin-bottom:0.2rem"><b>GPS:</b> 49.0981200N, 16.2374136E</p>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>