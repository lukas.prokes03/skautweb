# SkautWeb



## Hardwarové a softwarové požadavky

- Počítač s připojením na internet
- XAMPP s moduly Apache a MySQL (otestováno ve verzi 3.2.4)   
- Webový prohlížeč   
- Editor zdrojového kódu (například Visual Studio Code)  



## Manuál spuštění webové aplikace na lokálním serveru

1. Stáhněte si zdrojové kódy

2. Stáhněte si sql soubor skautweb, ve kterém je exportována databáze

3. Spusťte aplikaci XAMPP

4. Otevřte si adresář aplikace XAMPP a otevřte složku htdocs

5. Smažte její stávající obsah a vložte zdrojové kódy, které jste si v prvním bodě stáhli

6. V XAMPP control panelu spusťte moduly Apache a MySQL

7. Otevřte si váš webový prohlížeč

8. Do url adresy zadejte localhost/phpmyadmin

9. Vytvořte si tabulku s názvem skautweb a kódováním utf8mb4_general_ci

10. Do tabulky importujte sql soubor, který jste si stáhli ve druhém bodě. Nastavte znakovou sadu na utf-8

11. Otevřte si projekt ve vašem editoru zdrojového kódu

12. Proveďte potřebné úpravy v kódů, popsané níže

13. Do url adresy ebového prohlížeče zadejte localhost

14. Pro správu stránek přejděte na localhost/login

15. Přihlašte se na účet s nejvyšími právy root.   

    Přihlašovací údaje jsou:    
    - Uživatelské jméno: root   
    - Heslo: root

16. Přečtě si níže uvedený manuál pro správu obsahu

17. Spravujte obsah stránek


## Úpravy v kódu potřebné pro úspěšné spuštění webové aplikace

1. Zkontrolujte, případně změňte hodnotu promněné $webDomainName. Ta uchovává název domény, na které je web přístupný. V tomto případě http://localhost     
    - Nachází se v souboru imports/client.php line 4
2. Zkontrolujte, případně změňte přístupové údaje do databáze  
    - V souboru admin/config.php
2. Doplňte přihlašovací údaje od e-mailové adresy, ze které se budou e-maily posílat.
    - V souboru imports/phpMailer.php line 14 a 15


## Manuál pro správu obsahu

Spuštění webu pomocí softwaru XAMPP je nejpřesnější možná simulace ostrého nasazení webu. Díky tomu jsou přístupné a funkční všechny možnosti správy.

Je tu ovšem prvek, který při použití lokálního serveru nefunguje, a to jsou obrázky v rozesílaných e-mailech. Z důvodu, že se e-mailový klient neumí odkázat na obrázky hostované na adrese http://localhost.

Vetšina funkcí správy webu je přístupná pro předvytvořený účet root, až na možnost změny hesla, pro kterou je potřeba reálná e-mailová adresa, do které 
má uživatel přístup. Z toho důvodu, pro otestování této funkce si vytvořte nový úživatelský účet s existující e-mailovou adresou. 











  



