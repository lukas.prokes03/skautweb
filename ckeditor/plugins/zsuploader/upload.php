<?php
if (!isset($_SESSION)) session_start(); 
if(isset($_SESSION['upload_adr'])) $full_adr=$_SESSION['upload_adr'];
else $full_adr="./images/Lessons/";
if(is_array($_FILES)) {
if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
//$sourcePath = $_FILES['userImage']['tmp_name'];
//Change THIS target path if you need to
//$targetPath = $full_adr.$_FILES['userImage']['name'];

$target_dir = $full_adr;

$uploadOk = 1;
$imageFileType = strtolower(pathinfo($_FILES["userImage"]["name"],PATHINFO_EXTENSION));
$imageName = generateRandomName();
$fileName = $imageName . "." . $imageFileType;
$target_file = $target_dir . $imageName .".". $imageFileType;

//Kontrola existence $target_dir
if (!file_exists($target_dir)) {
    $uploadOk = 0;
}

// Check if file already exists
if (file_exists($target_file)) {
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    //echo ("Něco se nepovedlo");
} else {
    if($imageFileType == "jpg" || $imageFileType == "jpeg" ) {
        $orientation = exif_read_data($_FILES["userImage"]["tmp_name"])["Orientation"];
    }else{
        $orientation = 1;
    }

    list($width, $height, $type) = getimagesize($_FILES["userImage"]["tmp_name"]);
    $old_image = load_image($_FILES["userImage"]["tmp_name"], $type);
    
    if ($width>$height){
        $image_fixed = resize_image_to_width(700, $old_image, $width, $height);
    }else{
        $image_fixed = resize_image_to_height(700, $old_image, $width, $height);
    }
    imagejpeg($image_fixed, $target_file, 100);
    correctImageOrientation($target_file,$orientation);
    imagedestroy($image_fixed);

}

/*
//echo $sourcePath . "<br>";
//echo $full_adr;

$filename = $_FILES['userImage']['name'];
$loc = $targetPath;
if(file_exists($loc)){
    $increment = 0;
    list($name, $ext) = explode('.', $loc);
    while(file_exists($loc)) {
        $increment++;
        // $loc is now "userpics/example1.jpg"
        $loc = $name. $increment . '.' . $ext;
        $filename = $name.'-'. $increment . '.' . $ext;
        $targetPath = $filename;
    }
}

$imageFileType = strtolower(pathinfo($_FILES['userImage']["name"],PATHINFO_EXTENSION));

$fileName = generateRandomName() . "." . $imageFileType;
//echo $imageFileType;

$adressToSave = $full_adr . $fileName;

//echo($adressToSave);


if(move_uploaded_file($sourcePath,$adressToSave)) {
	//chmod($targetPath,0777);

*/

echo '<img class="image-preview" src="<?php echo $target_file; ?>" class="upload-preview" />';

?>
<script>
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' ) ;
        var match = window.location.search.match(reParam) ;

        return ( match && match.length > 1 ) ? match[ 1 ] : null ;
    }
    //location.href = "/ckeditor/plugins/zsuploader/uploader.php";
    useImage("/public/img/article/<?php echo $fileName ?>","<?php echo $_SERVER['SERVER_NAME']; ?>");

    function useImage(imgSrc,server) {
        var funcNum = getUrlParam("CKEditorFuncNum");
        var imgSrc = imgSrc;
        var fileUrl = imgSrc;
        window.opener.CKEDITOR.tools.callFunction(funcNum, 'http://'+server+fileUrl );

        window.close();
    }

</script>
<?php
}
}




function generateRandomName(){
    $length = 8;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $real_string_length = strlen($characters) ;     
    $string="";

    for ($p = 0; $p < $length; $p++){
        $string .= $characters[mt_rand(0, $real_string_length-1)];
    }
    return $string;
}
function correctImageOrientation($filename,$orientation) {
    if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
        case 3:
            $deg = 180;
            break;
        case 6:
            $deg = 270;
            break;
        case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
        $img = imagerotate($img, $deg, 0);        
        }
        imagejpeg($img, $filename, 95);
    }  
}

function load_image($filename, $type) {
    if( $type == IMAGETYPE_JPEG ) {
        $image = imagecreatefromjpeg($filename);
    }
    elseif( $type == IMAGETYPE_PNG ) {
        $image = imagecreatefrompng($filename);
    }
    elseif( $type == IMAGETYPE_GIF ) {
        $image = imagecreatefromgif($filename);
    }
    return $image;
}

function resize_image($new_width, $new_height, $image, $width, $height) {
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    return $new_image;
}

function resize_image_to_width($new_width, $image, $width, $height) {
    $resize_ratio = $new_width / $width;
    $new_height = $height * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}

function resize_image_to_height($new_height, $image, $width, $height) {
    $resize_ratio = $new_height / $height;
    $new_width = $width * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}
?>
