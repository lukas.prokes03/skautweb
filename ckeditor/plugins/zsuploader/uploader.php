<?php
error_reporting(E_ALL);

if (!isset($_SESSION)) session_start();
$version="1.2";
$_SESSION['zsuploader_version']=$version;
include_once('version_checker.php');
// And now, magic by zmmaj from ZmajSoft
//This script is free, so uset it as you wish.
$server= $_SERVER['SERVER_NAME'];
//echo $_SERVER["DOCUMENT_ROOT"];
//please change this address if you need some orher place.
//please consider that you have '/' on start and on end off address.
$upload_folder="/public/img/article/";

$full_adr=$server.$upload_folder;
$full_browse_dir=$_SERVER["DOCUMENT_ROOT"].$upload_folder;
$_SESSION['upload_adr']=$full_browse_dir;


//Stuff related to my site
//You can delete this, if you do not need
//******************************************************
if(isset($_SESSION["id"])){ $ses_id=$_SESSION["id"];
if(!file_exists($full_browse_dir."/".$ses_id))
{ mkdir($full_browse_dir."/".$ses_id, 0777, true);
}
$full_adr=$full_adr.$ses_id."/";
$full_browse_dir=$full_browse_dir.$ses_id."/";
$upload_folder=$upload_folder.$ses_id."/";
$_SESSION['upload_adr']=$full_browse_dir;
}

$itemsPerPage = '16';


//*******************************************************
function check_jpeg($f, $fix=false ){
# [070203]
# check for jpeg file header and footer - also try to fix it
    if ( false !== (@$fd = fopen($f, 'r+b' )) ){
        if ( fread($fd,2)==chr(255).chr(216) ){
            fseek ( $fd, -2, SEEK_END );
            if ( fread($fd,2)==chr(255).chr(217) ){
                fclose($fd);
                return true;
            }else{
                if ( $fix && fwrite($fd,chr(255).chr(217)) ){return true;}
                fclose($fd);
                return false;
            }
        }else{fclose($fd); return false;}
    }else{
        return false;
    }
}
//*******************************************************
function imageCreateFromAny($filepath) {
    $type = exif_imagetype($filepath); // [] if you don't have exif you could use getImageSize()
    $allowedTypes = array(
        1,  // [] gif
        2,  // [] jpg
        3,  // [] png
        4,  // [] jpeg
        6   // [] bmp
    );
    if (!in_array($type, $allowedTypes)) {
        return false;
    }
    switch ($type) {
        case 1 :
            $im = imageCreateFromGif($filepath);
        break;
        case 2 :
            $im = imageCreateFromJpeg($filepath);
        break;
        case 3 :
        
            $im = imageCreateFromPng($filepath);
        break;
        case 6 :
            $im = imageCreateFromBmp($filepath);
        break;
    }   
    return $im; 
} 
// create thumbnails from images
/*
function make_thumb($folder,$src,$dest,$thumb_width) {
//if (check_jpeg($folder.'/'.$src)){
	$source_image = imageCreateFromAny($folder.'/'.$src);
//	$source_image = imagecreatefromjpeg($folder.'/'.$src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	$thumb_height = floor($height*($thumb_width/$width));
	
	$virtual_image = imagecreatetruecolor($thumb_width,$thumb_height);
	
	imagecopyresampled($virtual_image,$source_image,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
	
	imagejpeg($virtual_image,$dest,100);
	
}
*/
//}

// display pagination
function print_pagination($numPages,$currentPage) {
   echo '<nav aria-label="Navigace" class="mt-4">
   <ul class="pagination justify-content-center">';
   
   if ($numPages > 1) {
      
	   echo '&nbsp;&nbsp;';
       if ($currentPage > 1) {
	       $prevPage = $currentPage - 1;
           echo '<li class="page-item">
        <a class="page-link" href="'. $_SERVER['PHP_SELF'] .'?p='. $prevPage.'" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
        </li>';
	       //echo '<a href="'. $_SERVER['PHP_SELF'] .'?p='. $prevPage.'">&laquo;&laquo;</a>';
       }

       
	   
	   for( $e=0; $e < $numPages; $e++ ) {
           $p = $e + 1;
       
	       if ($p == $currentPage) {	    
		       $class = 'current-paginate';
	       } else {
	           $class = 'paginate';
	       } 
	       

		       //echo '<a class="'. $class .'" href="'. $_SERVER['PHP_SELF'] .'?p='. $p .'">'. $p .'</a>';
               echo '<li class="page-item"><a class="page-link" href="'. $_SERVER['PHP_SELF'] .'?p='. $p .'">'.$p.'</a></li>';
		  	  
	   }
	   
	   if ($currentPage != $numPages) {
           $nextPage = $currentPage + 1;	
           echo '<li class="page-item">
        <a class="page-link" href="'. $_SERVER['PHP_SELF'] .'?p='. $nextPage.'" aria-label="Previous">
            <span aria-hidden="true">&raquo;</span>
        </a>
        </li>';
       }	 
   
   }

   echo "  </ul>
   </nav>";

}
?>

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Nahrání obrázků</title>
    <meta name="author" content="ZmajSOft">
    
    <link href="styles.css" rel="stylesheet" type="text/css" />



<?php 
include "../../../imports/head.php"
?>
</head>

<body>
<?php 
include "../../../imports/body.php"
?>
<table width="100%">
    <tr>
        <td class="bg-white px-4">
            <form id="uploadForm" action="upload.php?CKEditorFuncNum=<?php echo $_REQUEST["CKEditorFuncNum"] ?>" class="mt-4" method="post" enctype="multipart/form-data" onsubmit="document.getElementById('submitButton').style.display='none'">
                <div id="targetLayer"></div>
                    <div id="uploadFormLayer">
                        <label class="form-label" for="customFile">Nahrát obrázek</label>
                        <input name="userImage" accept="image/*" type="file" class="inputFile form-control" onchange="preview_image(event)" required/> <br>
                        <input type="submit" value="Odeslat" class="btnSubmit"  id="submitButton"/>
                    </div>
                </div>
            </form>
        </td>
    </tr>
    <tr>
        <td>
<?php          // number of images per page    
$thumb_width  = '150';        // width of thumbnails
$thumb_height = '130';         // height of thumbnails
$src_folder   =$full_browse_dir;             // current folder
$src_files    = scan_dir($src_folder); // files in current folder
$extensions   = array(".jpg",".jpeg",".png",".gif",".JPG",".JPEG",".PNG",".GIF"); // allowed extensions in photo gallery

function scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}

$files = array();
$Ftype="Images";
$CKEditorFuncNum=0;
If(isset($_REQUEST['CKEditorFuncNum'])) $CKEditorFuncNum= $_REQUEST['CKEditorFuncNum'];

foreach($src_files as $file) {
        
	$ext = strrchr($file, '.');
    if(in_array($ext, $extensions)) {
          
       array_push( $files, $file );
		  
        /*
       if (!is_dir($src_folder.'/thumbs')) {
          mkdir($src_folder.'/thumbs');
          chmod($src_folder.'/thumbs', 0777); 
       }
       
		   
	   $thumb = $src_folder.'/thumbs/'.$file;
       if (!file_exists($thumb)) {
          make_thumb($src_folder,$file,$thumb,$thumb_width); 
       chmod($thumb, 0777); 
	   }
        */
	}
      
}



if ( count($files) == 0 ) {

    echo 'There are no photos in this album!';
   
} else {
    echo '<div class="px-4 py-2 float-left">'.count($files).' obrázků</div>';
    $numPages = ceil( count($files) / $itemsPerPage );

    if(isset($_GET['p'])) {
      
       $currentPage = $_GET['p'];
       if($currentPage > $numPages) {
           $currentPage = $numPages;
       }

    } else {
       $currentPage=1;
    } 

    $start = ( $currentPage * $itemsPerPage ) - $itemsPerPage;
    
    echo '<div class="gallery px-4">';

    for( $i=$start; $i<$start + $itemsPerPage; $i++ ) {
		  
	   if( isset($files[$i]) && is_file( $src_folder .'/'. $files[$i] ) ) { 
	   //$slika=$files[$i]; 
	   $full_slika=$upload_folder.$files[$i]; //image
	   //$za_brisanje=$src_folder .'/'. $files[$i];
	   //$za_brisanje_thumb=$src_folder .'/thumbs/'. $files[$i];
	   //$thumb=$upload_folder."/thumbs/".$files[$i]; 
	  ?>
	   <div class="thumb">
		    <!--<span class="closes" title="Delete" onclick="deleteImage('<?php echo $za_brisanje;?>',1);deleteImage('<?php echo $za_brisanje_thumb;?>',2);" >&times;</span>-->
            
            <!-- <div class="imageThumbnail" style="background-image:url('<?php echo $thumb?>')"   -->
            <div class="imageThumbnail" style="background-image:url('<?php echo $full_slika?>')" 
            onclick="return false"  ondblclick="useImage('<?php echo $full_slika ?>','<?php echo $server?>');" >
            </div>
             
		</div>
	<?php
      
	    } else {
		  
		  if( isset($files[$i]) ) {
		    echo $files[$i];
		  }
		  
		}
     
    }
	echo "</div>";
        	 
    print_pagination($numPages,$currentPage);

}
?>
</td></tr>  </table>


<script>
function useImage(imgSrc,server) {
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' ) ;
        var match = window.location.search.match(reParam) ;

        return ( match && match.length > 1 ) ? match[ 1 ] : null ;
    }
    var funcNum = getUrlParam("CKEditorFuncNum");
    var imgSrc = imgSrc;
    var fileUrl = imgSrc;
    window.opener.CKEDITOR.tools.callFunction(funcNum, 'http://'+server+fileUrl );

    window.close();
}

function deleteImage(file_name,no)
{
	if (no==1){
    var r = confirm("Are you sure you want to delete this Image?")
    if(r == true)
    {
        $.ajax({
          url: 'delete.php',
          type: "POST",
          data: {'file' : file_name },
          success: function (response) {
			  alert(response);
             // do something
          },
          error: function () {
             // do something
             alert("something wrong");
          }
        });
    }
}
if (no==2) location = location;

}

$(document).ready(function (e) {
	$("#uploadForm").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "upload.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#targetLayer").html(data);
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function preview_image(event) {
 var reader = new FileReader();
 reader.onload = function()
 {
     console.log(reader.result);
  var output = document.getElementById('targetLayer');
  output.style["background-image"] = "url('"+reader.result+"')";
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>




</body>
</html>
