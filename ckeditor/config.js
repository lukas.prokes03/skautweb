/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	//config.filebrowserUploadUrl  = '/ckeditor/plugins/image/plugin.js';
	//config.filebrowserImageUploadUrl = '/ckeditor/plugins/image/plugin.js';

    config.filebrowserBrowseUrl = '/ckeditor/plugins/zsuploader/uploader.php';

	// Define changes to default configuration here. For example:
	config.language = 'cs';
	config.uiColor = '#FEF9F1';

	//config.enterMode = CKEDITOR.ENTER_BR;

    config.extraPlugins = "image2,autogrow,uploadimage";
	//config.extraPlugins = 'uploadimage,image2,autogrow,zsuploader';
	config.uploadUrl = '../admin/ckUpload.php';
	

	

	config.autoGrow_minHeight = 250;
	config.autoGrow_maxHeight = 2000;

	config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'undo', 'clipboard' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'paragraph', groups: [ 'align', 'list', 'indent', 'blocks', 'bidi', 'paragraph' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Language,BidiRtl,BidiLtr,CreateDiv,Blockquote,RemoveFormat,CopyFormatting,Cut,Copy,Paste,PasteText,PasteFromWord,SelectAll,Find,Replace,Save,NewPage,Preview,Print,ExportPdf,Templates,Anchor,Flash,PageBreak,ShowBlocks,Maximize,About';
};
