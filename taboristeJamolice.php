<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php") ?>
    <link rel="stylesheet" href="public/css/carousel.css" />
    <title>Skaut Domašov | Kontakt</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("view/indexHeader.php") ?>
            <div class="page_container">
                <p class="nazev_klubovny">Tábořiště u Jamolic</p>
                <div class="row">
                    <div class="col-md-6" >
                        <p>Od r. 2017 máme tábořiště na břehu řeky Jihlavy, nedaleko obce Jamolice a zříceniny hradu Templštejn. Kolem se nachází 
                            krásná čistá příroda, lesy, v průzračně čisté říční vodě lze někdy spatřit raky. <br><br>Pitná voda je dostupná z asi 200m 
                            vzdálené studánky. Sociální zázemí po celý rok zajišťují suché záchody. <br> Po dohodě je možné si louku půjčit či 
                            pronajmout. Během prázdnin na táboření i včetně vybavení: <br>
                            <ul>
                                <li>21 podsadových stanů</li>
                                <li>Tee-pee průměr 6.5m</li>
                                <li>Hangár 6x8m</li>
                                <li>Táborová kuchyň a jídelna (pro cca 40 lidí) </li>
                                <li>Kamna na dřevo s integrovaným ohřevem vody</li>
                                <li>Kuchyňské vybavení (hrnce, pánve, mísy, příbory, …)</li>
                                <li>Základní nářadí (pily, sekery, kladivo, …)</li>
                                <li>Sprcha s průtokovým ohřevem na dřevo</li>
                                <li>Barely na vodu</li>
                            </ul>
                            Pro podrobnější informace o vybavení, půjčení nebo termínu nám napište e-mail na skaut.domasov@seznam.cz.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <iframe style="border:none" src="https://en.frame.mapy.cz/s/nasefosoja" width="100%" height="450px" frameborder="0"></iframe>              
                    </div>
                </div>
                <div style="width:80%;margin:auto;padding-bottom: 4rem;">
                    <div class="slideshow-container fade">
                        <div class="Containers">
                            <img src="public/img/taboriste_jamolice/taboriste1.JPG" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/taboriste_jamolice/taboriste2.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/taboriste_jamolice/taboriste3.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/taboriste_jamolice/taboriste4.jpg" style="width:100%">
                        </div>
                        <div class="Containers">
                            <img src="public/img/taboriste_jamolice/taboriste5.JPG" style="width:100%">
                        </div>
                        <a class="Back" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="forward" onclick="plusSlides(1)">&#10095;</a>
                    </div><br>
                    <div style="text-align:center">
                        <span class="dots" onclick="currentSlide(1)"></span>
                        <span class="dots" onclick="currentSlide(2)"></span>
                        <span class="dots" onclick="currentSlide(3)"></span>
                        <span class="dots" onclick="currentSlide(4)"></span>
                        <span class="dots" onclick="currentSlide(5)"></span>
                    </div> 
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
    <script>
        var slidePosition = 1;
        SlideShow(slidePosition);

        function plusSlides(n) {
            SlideShow(slidePosition += n);
        }
        function currentSlide(n) {
            SlideShow(slidePosition = n);
        }
        function SlideShow(n) {
            var i;
            var slides = document.getElementsByClassName("Containers");
            var circles = document.getElementsByClassName("dots");
            if (n > slides.length) {slidePosition = 1}
            if (n < 1) {slidePosition = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < circles.length; i++) {
                circles[i].className = circles[i].className.replace(" enable", "");
            }
            slides[slidePosition-1].style.display = "block";
            circles[slidePosition-1].className += " enable";
        } 
    </script>
  </body>
</html>