<?php include ("imports/client.php") ?>

<!DOCTYPE html>
<html lang="cs">
  <head>
    <?php include("imports/head.php")  ?>
    <title>Skaut Domašov | Roveři</title>
  </head>
  <body>
    <?php include("imports/body.php") ?>
    <div class="page">
        <div class="container-fluid" >
            <?php include("./view/indexHeader.php") ?>
            <div class="page_container">
                <div class="row">
                    <div class="col-md-8 ">
                        <img src="public/img/roveri.png" width="100%" alt="" style="margin-bottom:1rem" >
                        <b class="nazevDruziny">Roveři</b><br><br>
                        <p class="pageData">
                            Roveři a rangers jsou starší a zkušenější skauti a skautky, kteří se často podílí na tvorbě a realizaci 
                            programů pro mladší brášky a sestřičky. Věkově se jedná o skauty a skautky přibližně ve věku od patnácti do třiceti let. <br>
                            <br> Roverský program si připravujeme sami - sobě navzájem. Chápeme, že už jsme (skoro) dospělí a tomu opovídá i náplň 
                            programu. Snažíme se pomáhat, kde můžeme (byli jsme pomoci na jižní moravě po ničivém tornádu, sekáme a uklízíme farní 
                            zahradu, …), ale i tak máme dost prostoru pro zábavu a naše zájmy: zajímavé naučné přednášky, turistika, filmy, vyrábění 
                            všeho druhu, horolezení, tancování, neobvyklé sporty apod. <br><br> Schůzky nemíváme pravidelně, ale snažíme se setkávat 
                            na různých akcích, a pokud žádné nejsou, tak se někdy sejdeme i jen tak, třeba na deskovky.<br><br> 
                            Pokud se chceš přidat, napiš nebo zavolej Dobkovi nebo Vojtovi, rádi do party přibereme nové a zajímavé lidi!<br><br>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="blurBackground vedouci_druziny">
                            <div class="vedouci">
                                <p style="font-family: Arial, Helvetica, sans-serif;font-size: 1.25rem;text-align: center">
                                    <b class="nazevDruziny">Vedoucí družiny</b>
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/jirka.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Jiří Liška</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 737 432 209<br>
                                    e-mail: drobecek@skaut.cz
                                </p>
                                <div class="text-center">
                                    <img src="public/img/vedouci/vojta.png" width="45%" alt="" class="rounded" style="margin-bottom:1rem;margin-top:1.2rem">
                                </div>
                                <b class="vedouciDruziny">Vojta Staněk</b><br><br>
                                <p class="vedouciDruzinyData">
                                    mobil: +420 601 507 845 <br>
                                    e-mail: vstanek@skaut.cz
                                </p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
        <?php include("view/indexFooter.php") ?>
    </div>
  </body>
</html>